package br.com.talonario.ws.resources;

import java.io.Serializable;

import javax.inject.Inject;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;

import br.com.talonario.entities.CustomResponseEntity;
import br.com.talonario.entities.PackageNumberAutos;
import br.com.talonario.services.PackageNumberService;

@Path( "/package" )
public class PackageNumberAutosResource implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -8982223346654515678L;

	public static final Logger logger = LoggerFactory.getLogger( PackageNumberAutosResource.class );

	@Inject
	private PackageNumberService packageNumberService;

	@Path( "/load" )
	@GET
	@Produces( MediaType.APPLICATION_JSON + "; charset=UTF-8" )
	public CustomResponseEntity load( @QueryParam( "organ" ) String organName, @QueryParam( "codeOrgan" ) Integer codeOrgan ) {

		PackageNumberAutos packageNumberAutos = packageNumberService.loadPackageNumber( organName, codeOrgan );

		if ( packageNumberAutos != null )
			logger.info( "PackageNumber: init[{}] end[{}] user[{}]", packageNumberAutos.getInitNumber().toString(), packageNumberAutos.getEndNumber().toString(), packageNumberAutos.getUsernameAgent() );
		return new CustomResponseEntity( HttpStatus.OK.value(), packageNumberAutos );

	}
}
