package br.com.talonario.ws.resources;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.hibernate.Hibernate;
import org.hibernate.proxy.HibernateProxy;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

import br.com.talonario.entities.City;
import br.com.talonario.entities.CustomResponseEntity;
import br.com.talonario.entities.Device;
import br.com.talonario.entities.Organ;
import br.com.talonario.entities.State;
import br.com.talonario.entities.User;
import br.com.talonario.services.DeviceService;
import br.com.talonario.services.TokenService;
import br.com.talonario.services.UserService;

@Path( "/user" )
public class UserResource implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 4650201327115144202L;

	public static final Logger logger = LoggerFactory.getLogger( UserResource.class );

	@Inject
	private UserService userService;

	@Autowired
	private AuthenticationManager authenticationManager;

	@Inject
	private DeviceService deviceService;

	@Inject
	private TokenService tokenUtils;

	@Path( "/login" )
	@POST
	@Consumes( MediaType.APPLICATION_JSON )
	@Produces( MediaType.APPLICATION_JSON + "; charset=UTF-8" )
	public CustomResponseEntity login( User user ) {
		BCryptPasswordEncoder passwordEncoder = new BCryptPasswordEncoder( 10 );

		logger.info( "Login APP User [{}]...", user.getUsername() );
		Device device = deviceService.findDeviceByImei( user.getImei() );
		User loadedUser = userService.loginApp( user.getUsername() );
		if ( loadedUser != null && loadedUser.isEnabled() && passwordEncoder.matches( user.getPassword(), loadedUser.getPassword() ) ) {
			if ( device != null ) {
				Device otherDevice = deviceService.findDeviceByAgentUsername( user.getUsername() );
				if ( otherDevice == null || ( otherDevice != null && otherDevice.getImei().equals( device.getImei() ) ) ) {
					String token = tokenUtils.generateToken( user.getUsername(), user.getCreatedAt() );
					loadedUser.setToken( token );
					userService.save( loadedUser );

					Set<Organ> organs = new HashSet<Organ>();
					Set<City> cities = new HashSet<City>();
					State state = new State();
					for ( Organ organ : loadedUser.getOrgans() ) {
						for ( City city : organ.getCities() ) {
							state = city.getState();
							Hibernate.initialize( state );
							state = (State) ( (HibernateProxy) state ).getHibernateLazyInitializer().getImplementation();
							city.setState( state );
							cities.add( city );
						}
						organ.setCities( cities );
						organs.add( organ );
					}
					loadedUser.setOrgans( organs );
					device.setAgentUsername( loadedUser.getUsername() );
					deviceService.save( device );

					logger.info( "Logged User [{}]...", user.getUsername() );
					return new CustomResponseEntity( HttpStatus.OK.value(), loadedUser );
				} else
					return new CustomResponseEntity( HttpStatus.NOT_ACCEPTABLE.value(), "Usuário está em uso por outro dispositivo" );
			} else
				return new CustomResponseEntity( HttpStatus.NOT_ACCEPTABLE.value(), "Dispositivo inválido" );
		} else
			return new CustomResponseEntity( HttpStatus.NOT_ACCEPTABLE.value(), "Usuário ou senha incorretos" );
	}

	@Path( "/logout" )
	@POST
	@Consumes( MediaType.APPLICATION_JSON )
	@Produces( MediaType.APPLICATION_JSON + "; charset=UTF-8" )
	public CustomResponseEntity logout( User user ) {
		Device device = deviceService.findDeviceByImeiAndAgentUsername( user.getImei(), user.getUsername() );
		device.setAgentUsername( null );
		deviceService.save( device );
		return new CustomResponseEntity( HttpStatus.OK.value() );
	}

}
