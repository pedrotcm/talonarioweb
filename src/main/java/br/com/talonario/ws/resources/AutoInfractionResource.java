package br.com.talonario.ws.resources;

import java.io.Serializable;
import java.util.List;

import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;

import br.com.talonario.entities.AutoInfraction;
import br.com.talonario.entities.CustomResponseEntity;
import br.com.talonario.entities.Organ;
import br.com.talonario.entities.PackageNumberAutos;
import br.com.talonario.services.AutoInfractionService;
import br.com.talonario.services.ImageService;
import br.com.talonario.services.OrganService;
import br.com.talonario.services.PackageNumberService;

@Path( "/auto" )
public class AutoInfractionResource implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 2011014717774860073L;

	public static final Logger logger = LoggerFactory.getLogger( AutoInfractionResource.class );

	@Inject
	private AutoInfractionService autoInfractionService;

	@Inject
	private ImageService imageService;

	@Inject
	private PackageNumberService packageNumberService;

	@Inject
	private OrganService organService;

	// @Path( "/send" )
	// @POST
	// @Consumes( MediaType.APPLICATION_JSON )
	// @Produces( MediaType.APPLICATION_JSON + "; charset=UTF-8" )
	// public CustomResponseEntity send( AutoInfraction auto ) {
	//
	// imageService.save( auto.getImages() );
	// autoInfractionRepository.save( auto );
	//
	// if ( auto.getId() != null )
	// return new CustomResponseEntity( HttpStatus.CREATED.value() );
	// else
	// return new CustomResponseEntity( HttpStatus.NOT_ACCEPTABLE.value() );
	// }

	@Path( "/send" )
	@POST
	@Consumes( MediaType.APPLICATION_JSON )
	@Produces( MediaType.APPLICATION_JSON + "; charset=UTF-8" )
	public CustomResponseEntity send( AutoInfraction auto ) {

		logger.info( "Sending Auto: {}", auto.getNumberAutoInfraction() );
		Organ organ = organService.findOrganByNameAndCode( auto.getOrgan(), auto.getCodeOrgan() );
		List<PackageNumberAutos> packageNumber = packageNumberService.findByOrganAndUsernameAgentOrderByRequestDateDesc( organ, auto.getAgentUsername() );
		String numberAuto = auto.getNumberAutoInfraction().substring( 2 );
		packageNumber.get( 0 ).setCurrentNumber( new Integer( numberAuto ) );

		if ( auto.getIdWeb() != null ) {
			AutoInfraction autoLoaded = autoInfractionService.load( auto.getIdWeb() );
			autoLoaded.setCancel( auto.getCancel() );
			autoInfractionService.save( autoLoaded );
			return new CustomResponseEntity( HttpStatus.CREATED.value(), autoLoaded.getId() );
		} else {
			imageService.save( auto.getImages() );
			autoInfractionService.save( auto );
		}

		if ( auto.getId() != null ) {
			packageNumberService.save( packageNumber );
			return new CustomResponseEntity( HttpStatus.CREATED.value(), auto.getId() );
		} else
			return new CustomResponseEntity( HttpStatus.NOT_ACCEPTABLE.value() );
	}

	// @Path( "/sendAll" )
	// @POST
	// @Consumes( MediaType.APPLICATION_JSON )
	// @Produces( MediaType.APPLICATION_JSON + "; charset=UTF-8" )
	// public CustomResponseEntity sendAutos( AutoInfractionWrapper wrapper ) {
	//
	// Map<String, Integer> autoIdsReturn = new HashMap<String, Integer>();
	// for ( AutoInfraction auto : wrapper.getAutoInfractions() ) {
	// logger.info( "Sending Auto: {}", auto.getNumberAutoInfraction() );
	// Organ organ = organService.findOrganByNameAndCode( auto.getOrgan(),
	// auto.getCodeOrgan() );
	// List<PackageNumberAutos> packageNumber =
	// packageNumberService.findByOrganAndUsernameAgentOrderByRequestDateDesc(
	// organ, auto.getAgentUsername() );
	// String numberAuto = auto.getNumberAutoInfraction().substring( 2 );
	// packageNumber.get( 0 ).setCurrentNumber( new Integer( numberAuto ) );
	//
	// if ( auto.getIdWeb() != null ) {
	// AutoInfraction autoLoaded = autoInfractionService.load( auto.getIdWeb()
	// );
	// autoLoaded.setCancel( auto.getCancel() );
	// autoInfractionService.save( autoLoaded );
	// autoIdsReturn.put( auto.getNumberAutoInfraction(), autoLoaded.getId() );
	// } else {
	// imageService.save( auto.getImages() );
	// autoInfractionService.save( auto );
	// autoIdsReturn.put( auto.getNumberAutoInfraction(), auto.getId() );
	// }
	// }
	//
	// return new CustomResponseEntity( HttpStatus.CREATED.value(),
	// autoIdsReturn );
	// }
}
