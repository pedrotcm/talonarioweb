package br.com.talonario.ws.resources;

import java.io.Serializable;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import javax.inject.Inject;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import br.com.talonario.entities.AutoInfraction;
import br.com.talonario.services.AutoInfractionService;

@Path( "/resource" )
public class WebServiceResource implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -4415064739079353966L;

	public static final Logger logger = LoggerFactory.getLogger( WebServiceResource.class );

	@Inject
	private AutoInfractionService autoInfractionService;

	@Path( "/allautos" )
	@GET
	@Produces( MediaType.APPLICATION_JSON + "; charset=UTF-8" )
	public List<AutoInfraction> findAll() {
		logger.info( "WebService findAll" );
		return autoInfractionService.findAll();
	}

	@Path( "/autosBetweenInterval" )
	@GET
	@Produces( MediaType.APPLICATION_JSON + "; charset=UTF-8" )
	public List<AutoInfraction> autosByInterval( @QueryParam( "initDate" ) String initDate, @QueryParam( "endDate" ) String endDate ) {
		logger.info( "WebService autosByInterval" );

		Date init = getDate( initDate );
		Date end = getDate( endDate );
		if ( init != null && end != null )
			return autoInfractionService.findByCreatedAtBetween( init, end );

		return null;
	}

	@Path( "/autosAfterDate" )
	@GET
	@Produces( MediaType.APPLICATION_JSON + "; charset=UTF-8" )
	public List<AutoInfraction> autosAfterDate( @QueryParam( "initDate" ) String initDate ) {
		logger.info( "WebService autosAfterDate" );

		Date init = getDate( initDate );
		if ( init != null )
			return autoInfractionService.findByCreatedAtAfter( init );

		return null;
	}

	@Path( "/autosBeforeDate" )
	@GET
	@Produces( MediaType.APPLICATION_JSON + "; charset=UTF-8" )
	public List<AutoInfraction> autosBeforeDate( @QueryParam( "endDate" ) String endDate ) {
		logger.info( "WebService autosBerforeDate" );

		Date end = getDate( endDate );
		if ( end != null )
			return autoInfractionService.findByCreatedAtBefore( end );

		return null;
	}

	private Date getDate( String date ) {
		SimpleDateFormat dateFormat = new SimpleDateFormat( "yyyy-MM-dd HH:mm:ss" );
		try {
			if ( date != null )
				return dateFormat.parse( date );
		} catch ( ParseException e ) {
			e.printStackTrace();
		}
		return null;
	}
}
