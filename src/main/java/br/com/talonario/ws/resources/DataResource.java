package br.com.talonario.ws.resources;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;

import br.com.talonario.entities.Brand;
import br.com.talonario.entities.ContactOccurrence;
import br.com.talonario.entities.CustomResponseEntity;
import br.com.talonario.entities.Infraction;
import br.com.talonario.entities.MeasurementType;
import br.com.talonario.entities.Patio;
import br.com.talonario.entities.Reason;
import br.com.talonario.entities.SettingParameter;
import br.com.talonario.entities.Specie;
import br.com.talonario.services.BrandService;
import br.com.talonario.services.ContactOccurrenceService;
import br.com.talonario.services.InfractionService;
import br.com.talonario.services.MeasurementTypeService;
import br.com.talonario.services.PatioService;
import br.com.talonario.services.ReasonService;
import br.com.talonario.services.SettingParameterService;
import br.com.talonario.services.SpecieService;

@Path( "/data" )
public class DataResource<T> implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 2449103801228398532L;

	public static final Logger logger = LoggerFactory.getLogger( DataResource.class );

	@Inject
	private BrandService brandService;
	@Inject
	private SpecieService specieService;
	@Inject
	private ReasonService reasonService;
	@Inject
	private InfractionService infractionService;
	@Inject
	private SettingParameterService settingParameterService;
	@Inject
	private MeasurementTypeService measurementTypeService;
	@Inject
	private ContactOccurrenceService organOccurrenceService;
	@Inject
	private PatioService patioService;

	@SuppressWarnings( "unchecked" )
	@Path( "/load" )
	@GET
	@Produces( MediaType.APPLICATION_JSON + "; charset=UTF-8" )
	public CustomResponseEntity load() {
		logger.info( "Data Loading..." );

		ArrayList<ArrayList<T>> data = new ArrayList<ArrayList<T>>();

		List<Brand> brands = brandService.findAll();
		List<Specie> species = specieService.findAll();
		List<Reason> reasons = reasonService.findAll();
		List<Infraction> infractions = infractionService.findAll();
		List<SettingParameter> settings = settingParameterService.findAll();
		List<MeasurementType> measurementTypes = measurementTypeService.findAll();
		List<ContactOccurrence> organOccurrences = organOccurrenceService.findAll();
		List<Patio> patios = patioService.findAll();

		data.add( (ArrayList<T>) brands );
		data.add( (ArrayList<T>) species );
		data.add( (ArrayList<T>) reasons );
		data.add( (ArrayList<T>) infractions );
		data.add( (ArrayList<T>) settings );
		data.add( (ArrayList<T>) measurementTypes );
		data.add( (ArrayList<T>) organOccurrences );
		data.add( (ArrayList<T>) patios );

		return new CustomResponseEntity( HttpStatus.OK.value(), data );
	}
}
