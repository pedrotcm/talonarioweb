package br.com.talonario.ws.adapters;

import java.text.SimpleDateFormat;
import java.util.Date;

import javax.xml.bind.annotation.adapters.XmlAdapter;

public class JsonDateAdapter extends XmlAdapter<String, Date> {

	private SimpleDateFormat dateFormat = new SimpleDateFormat( "dd/MM/yyyy HH:mm" );

	@Override
	public Date unmarshal( String v ) throws Exception {
		Date data = dateFormat.parse( v );
		return data;
	}

	@Override
	public String marshal( Date v ) throws Exception {
		String data = dateFormat.format( v );
		return data;
	}

}
