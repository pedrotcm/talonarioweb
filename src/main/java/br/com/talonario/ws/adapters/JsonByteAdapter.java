package br.com.talonario.ws.adapters;

import java.nio.ByteBuffer;
import java.nio.CharBuffer;
import java.nio.charset.StandardCharsets;

import javax.xml.bind.annotation.adapters.XmlAdapter;

public class JsonByteAdapter extends XmlAdapter<String, byte[]> {

	@Override
	public byte[] unmarshal( String v ) throws Exception {
		byte[] b = v.getBytes( StandardCharsets.ISO_8859_1 );
		return b;
	}

	@Override
	public String marshal( byte[] v ) throws Exception {
		String s = new String( v, StandardCharsets.ISO_8859_1 );
		return s;
	}

	public static byte[] stringToBytesUTFNIO( String str ) {
		char[] buffer = str.toCharArray();
		byte[] b = new byte[buffer.length << 1];
		CharBuffer cBuffer = ByteBuffer.wrap( b ).asCharBuffer();
		for ( int i = 0; i < buffer.length; i++ )
			cBuffer.put( buffer[i] );
		return b;
	}

	public static String bytesToStringUTFNIO( byte[] bytes ) {
		CharBuffer cBuffer = ByteBuffer.wrap( bytes ).asCharBuffer();
		return cBuffer.toString();
	}

}
