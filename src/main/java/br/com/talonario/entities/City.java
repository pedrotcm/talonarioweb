package br.com.talonario.entities;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

@Entity
@Table( name = "municipio" )
@XmlRootElement
public class City extends BaseEntity implements Serializable {

	private static final long serialVersionUID = 1076859884207741886L;

	@Id
	@GeneratedValue( strategy = GenerationType.IDENTITY )
	@Column( name = "pk_id_municipio" )
	private Integer id;

	@Column( name = "nome", length = 50, nullable = false )
	private String name;

	@Column( name = "codigo_municipio" )
	private Integer cityCode;

	@OneToOne( fetch = FetchType.LAZY )
	@JoinColumn( name = "fk_id_estado" )
	private State state;

	public City() {}

	@Override
	public Integer getId() {
		return id;
	}

	@Override
	public void setId( Integer id ) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName( String name ) {
		this.name = name;
	}

	public Integer getCityCode() {
		return cityCode;
	}

	public void setCityCode( Integer cityCode ) {
		this.cityCode = cityCode;
	}

	public State getState() {
		return state;
	}

	public void setState( State state ) {
		this.state = state;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ( ( getId() == null ) ? 0 : getId().hashCode() );
		result = prime * result + ( ( getName() == null ) ? 0 : getName().hashCode() );
		return result;
	}

	@Override
	public boolean equals( Object obj ) {
		if ( this == obj )
			return true;
		if ( !super.equals( obj ) )
			return false;
		if ( getClass() != obj.getClass() )
			return false;
		City other = (City) obj;
		if ( getId() == null ) {
			if ( other.getId() != null )
				return false;
		} else if ( !getId().equals( other.getId() ) )
			return false;
		if ( getName() == null ) {
			if ( other.getName() != null )
				return false;
		} else if ( !getName().equals( other.getName() ) )
			return false;
		return true;
	}

}
