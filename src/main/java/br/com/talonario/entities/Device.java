package br.com.talonario.entities;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

@Entity
@Table( name = "dispositivo" )
@XmlRootElement
public class Device extends BaseEntity implements Serializable {

	private static final long serialVersionUID = -7665936647587405322L;

	@Id
	@GeneratedValue( strategy = GenerationType.IDENTITY )
	@Column( name = "pk_id_dispositivo" )
	private Integer id;

	@Size( max = 100 )
	@Column( nullable = false, unique = true )
	private String imei;

	@Column( name = "agente", unique = true )
	private String agentUsername;

	@Override
	public Integer getId() {
		return id;
	}

	@Override
	public void setId( Integer id ) {
		this.id = id;
	}

	public String getImei() {
		return imei;
	}

	public void setImei( String imei ) {
		this.imei = imei;
	}

	public String getAgentUsername() {
		return agentUsername;
	}

	public void setAgentUsername( String agentUsername ) {
		this.agentUsername = agentUsername;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ( ( agentUsername == null ) ? 0 : agentUsername.hashCode() );
		result = prime * result + ( ( id == null ) ? 0 : id.hashCode() );
		result = prime * result + ( ( imei == null ) ? 0 : imei.hashCode() );
		return result;
	}

	@Override
	public boolean equals( Object obj ) {
		if ( this == obj )
			return true;
		if ( !super.equals( obj ) )
			return false;
		if ( getClass() != obj.getClass() )
			return false;
		Device other = (Device) obj;
		if ( agentUsername == null ) {
			if ( other.agentUsername != null )
				return false;
		} else if ( !agentUsername.equals( other.agentUsername ) )
			return false;
		if ( id == null ) {
			if ( other.id != null )
				return false;
		} else if ( !id.equals( other.id ) )
			return false;
		if ( imei == null ) {
			if ( other.imei != null )
				return false;
		} else if ( !imei.equals( other.imei ) )
			return false;
		return true;
	}

}
