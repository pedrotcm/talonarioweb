package br.com.talonario.entities;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@Entity
@Table( name = "marca" )
@XmlRootElement
public class Brand extends BaseEntity implements Serializable {

	private static final long serialVersionUID = -9177314001772780530L;

	@XmlElement( name = "id_marca" )
	@Id
	@GeneratedValue( strategy = GenerationType.IDENTITY )
	@Column( name = "pk_id_marca" )
	private Integer id;

	@Column( name = "descricao" )
	private String description;

	public Brand() {}

	@Override
	public Integer getId() {
		return id;
	}

	@Override
	public void setId( Integer id ) {
		this.id = id;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription( String description ) {
		this.description = description;
	}

}
