package br.com.talonario.entities;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@Entity
@Table( name = "infracao" )
@XmlRootElement
public class Infraction extends BaseEntity implements Serializable {

	private static final long serialVersionUID = 8746214835164374594L;

	@XmlElement( name = "id_infracao" )
	@Id
	@GeneratedValue( strategy = GenerationType.IDENTITY )
	@Column( name = "pk_id_infracao" )
	private Integer id;

	@Column( name = "codigo_infracao" )
	private Integer codeInfraction;

	@Column( name = "desdobramento" )
	private Integer deployment;

	@Column( name = "descricao" )
	private String description;

	@Column( name = "amparo_legal" )
	private String legalSupport;

	@Column( name = "lei" )
	private String law;

	@Column( name = "penalidade" )
	private String punishment;

	@Column( name = "medida_administrativa" )
	private String administrativeMeasure;

	@Column( name = "tipo_medicao" )
	private String measurementType;

	@Column( name = "observacoes_um" )
	private String observationsOne;

	@Column( name = "observacoes_dois" )
	private String observationsTwo;

	@Column( name = "observacoes_tres" )
	private String observationsThree;

	@Column( name = "competencia" )
	private String competence;

	@Column( name = "filtro" )
	private String filter;

	public Infraction() {}

	@Override
	public Integer getId() {
		return id;
	}

	@Override
	public void setId( Integer id ) {
		this.id = id;
	}

	public Integer getCodeInfraction() {
		return codeInfraction;
	}

	public void setCodeInfraction( Integer codeInfraction ) {
		this.codeInfraction = codeInfraction;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription( String description ) {
		this.description = description;
	}

	public String getLegalSupport() {
		return legalSupport;
	}

	public void setLegalSupport( String legalSupport ) {
		this.legalSupport = legalSupport;
	}

	public String getPunishment() {
		return punishment;
	}

	public void setPunishment( String punishment ) {
		this.punishment = punishment;
	}

	public String getAdministrativeMeasure() {
		return administrativeMeasure;
	}

	public void setAdministrativeMeasure( String administrativeMeasure ) {
		this.administrativeMeasure = administrativeMeasure;
	}

	public String getMeasurementType() {
		return measurementType;
	}

	public void setMeasurementType( String measurementType ) {
		this.measurementType = measurementType;
	}

	public String getLaw() {
		return law;
	}

	public void setLaw( String law ) {
		this.law = law;
	}

	public String getObservationsOne() {
		return observationsOne;
	}

	public void setObservationsOne( String observationsOne ) {
		this.observationsOne = observationsOne;
	}

	public String getObservationsTwo() {
		return observationsTwo;
	}

	public void setObservationsTwo( String observationsTwo ) {
		this.observationsTwo = observationsTwo;
	}

	public String getObservationsThree() {
		return observationsThree;
	}

	public void setObservationsThree( String observationsThree ) {
		this.observationsThree = observationsThree;
	}

	public String getCompetence() {
		return competence;
	}

	public void setCompetence( String competence ) {
		this.competence = competence;
	}

	public Integer getDeployment() {
		return deployment;
	}

	public void setDeployment( Integer deployment ) {
		this.deployment = deployment;
	}

	public String getFilter() {
		return filter;
	}

	public void setFilter( String filter ) {
		this.filter = filter;
	}

}
