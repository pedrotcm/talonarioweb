package br.com.talonario.entities;

import java.util.Set;

import javax.persistence.CollectionTable;
import javax.persistence.Column;
import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

import org.hibernate.validator.constraints.NotBlank;
import org.springframework.security.core.GrantedAuthority;

@Entity
@Table( name = Profile.TABLE_NAME )
@XmlRootElement
public class Profile extends BaseEntity implements GrantedAuthority {

	static final String TABLE_NAME = "perfil";

	static final String ID_COLUMN_NAME = "pk_id_perfil";

	static final String DESCRIPTION_COLUMN_NAME = "descricao";

	static final String ROLE_COLUMN_NAME = "funcao";

	static final String MENU_ITEM_COLUMN_NAME = "nome_item_menu";

	private static final long serialVersionUID = -5654625116117129512L;

	@Id
	@GeneratedValue( strategy = GenerationType.IDENTITY )
	@Column( name = Profile.ID_COLUMN_NAME )
	private Integer id;

	@NotBlank
	@Column( name = Profile.ROLE_COLUMN_NAME, unique = true )
	private String role;

	@NotBlank
	@Column( name = Profile.DESCRIPTION_COLUMN_NAME )
	private String description;

	@ElementCollection( fetch = FetchType.EAGER )
	@CollectionTable( name = TABLE_NAME + "_menuitems", joinColumns = @JoinColumn( name = "fk_id_perfil" ) )
	@Column( name = MENU_ITEM_COLUMN_NAME )
	private Set<String> menuItems;

	public Profile() {}

	public Profile( String authority ) {
		setAuthority( authority );
	}

	@Override
	public Integer getId() {
		return id;
	}

	@Override
	public void setId( Integer id ) {
		this.id = id;
	}

	public void setAuthority( String authority ) {
		this.role = authority;
	}

	@Override
	public String getAuthority() {
		return role;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription( String description ) {
		this.description = description;
	}

	public Set<String> getMenuItems() {
		return menuItems;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ( ( role == null ) ? 0 : role.hashCode() );
		return result;
	}

	@Override
	public boolean equals( Object obj ) {
		if ( this == obj )
			return true;
		if ( getClass() != obj.getClass() )
			return false;
		Profile other = (Profile) obj;
		if ( role == null ) {
			if ( other.role != null )
				return false;
		} else if ( !role.equals( other.role ) )
			return false;
		return true;
	}

	@Override
	public String toString() {
		return description + " - " + role;
	}

}
