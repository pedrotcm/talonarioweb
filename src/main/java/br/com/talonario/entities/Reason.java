package br.com.talonario.entities;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@Entity
@Table( name = "motivo" )
@XmlRootElement
public class Reason extends BaseEntity implements Serializable {

	private static final long serialVersionUID = -7243954011443714562L;

	@XmlElement( name = "id_motivo" )
	@Id
	@GeneratedValue( strategy = GenerationType.IDENTITY )
	@Column( name = "pk_id_motivo" )
	private Integer id;

	@Column( name = "descricao" )
	private String description;

	public Reason() {}

	@Override
	public Integer getId() {
		return id;
	}

	@Override
	public void setId( Integer id ) {
		this.id = id;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription( String description ) {
		this.description = description;
	}

}
