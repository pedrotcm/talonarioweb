package br.com.talonario.entities;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import br.com.talonario.utils.Utils;

@Entity
@Table( name = "telefone_ocorrencia" )
@XmlRootElement
public class TelephoneOccurrence extends BaseEntity implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1739350916308582768L;

	@XmlElement( name = "id_telefone_ocorrencia" )
	@Id
	@GeneratedValue( strategy = GenerationType.IDENTITY )
	@Column( name = "pk_id_telefone_ocorrencia" )
	private Integer id;

	@Size( max = 20 )
	@Column( name = "telefone", nullable = false )
	private String telephone;

	@Column( name = "padrao" )
	private Boolean standard;

	public TelephoneOccurrence() {
		this.standard = false;
	}

	@Override
	public Integer getId() {
		return id;
	}

	@Override
	public void setId( Integer id ) {
		this.id = id;
	}

	public String getTelephone() {
		return telephone;
	}

	public void setTelephone( String telephone ) {
		this.telephone = telephone;
	}

	public Boolean getStandard() {
		return standard;
	}

	public void setStandard( Boolean standard ) {
		this.standard = standard;
	}

	@Override
	public String toString() {
		return telephone != null ? telephone.toString() : null;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ( ( telephone == null ) ? 0 : telephone.hashCode() );
		return result;
	}

	@Override
	public boolean equals( Object obj ) {
		if ( this == obj )
			return true;
		if ( !Utils.getEntityClass( getClass() ).equals( Utils.getEntityClass( obj.getClass() ) ) )
			return false;
		TelephoneOccurrence other = (TelephoneOccurrence) obj;
		if ( telephone == null ) {
			if ( other.telephone != null )
				return false;
		} else if ( !telephone.equals( other.telephone ) )
			return false;
		return true;
	}

}
