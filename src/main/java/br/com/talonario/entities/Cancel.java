package br.com.talonario.entities;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

@Entity
@Table( name = "cancelado" )
@XmlRootElement
public class Cancel extends BaseEntity implements Serializable {

	private static final long serialVersionUID = -8117020526597674921L;

	@Id
	@GeneratedValue( strategy = GenerationType.IDENTITY )
	@Column( name = "pk_id_cancelado" )
	private Integer id;

	@Column( name = "justificativa" )
	private String justification;

	@Column( name = "motivo" )
	private String reason;

	@Override
	public Integer getId() {
		return id;
	}

	@Override
	public void setId( Integer id ) {
		this.id = id;
	}

	public String getJustification() {
		return justification;
	}

	public void setJustification( String justification ) {
		this.justification = justification;
	}

	public String getReason() {
		return reason;
	}

	public void setReason( String reason ) {
		this.reason = reason;
	}

}
