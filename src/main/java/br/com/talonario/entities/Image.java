package br.com.talonario.entities;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;

import br.com.talonario.utils.Utils;
import br.com.talonario.ws.adapters.JsonByteAdapter;
import br.com.talonario.ws.adapters.JsonDateAdapter;

@Entity
@Table( name = "imagem" )
@XmlRootElement
public class Image extends BaseEntity implements Serializable {

	private static final long serialVersionUID = 5593059399562818893L;

	@Id
	@GeneratedValue( strategy = GenerationType.IDENTITY )
	@Column( name = "pk_id_imagem" )
	private Integer id;

	@Column( name = "nome", nullable = false )
	private String name;

	@Size( max = 255 )
	@Column( name = "caminho" )
	private String path;

	@Column( name = "extensao" )
	private String extension;

	@Column( name = "data_inclusao" )
	@Temporal( TemporalType.TIMESTAMP )
	private Date createdDate;

	@Size( max = 100 )
	@Column( name = "url" )
	private String url;

	@Transient
	private byte[] fileContent;

	public Image() {}

	@Override
	public Integer getId() {
		return id;
	}

	@Override
	public void setId( Integer id ) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName( String name ) {
		this.name = name;
	}

	public String getPath() {
		return path;
	}

	public void setPath( String path ) {
		this.path = path;
	}

	@XmlJavaTypeAdapter( value = JsonDateAdapter.class )
	public Date getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate( Date createdDate ) {
		this.createdDate = createdDate;
	}

	@XmlJavaTypeAdapter( value = JsonByteAdapter.class )
	public byte[] getFileContent() {
		return fileContent;
	}

	public void setFileContent( byte[] fileContent ) {
		this.fileContent = fileContent;
	}

	// public String getFileContent() {
	// return fileContent;
	// }
	//
	// public void setFileContent( String fileContent ) {
	// this.fileContent = fileContent;
	// }

	public String getExtension() {
		return extension;
	}

	public void setExtension( String extension ) {
		this.extension = extension;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl( String url ) {
		this.url = url;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ( ( id == null ) ? 0 : id.hashCode() );
		result = prime * result + ( ( createdDate == null ) ? 0 : createdDate.hashCode() );
		result = prime * result + ( ( extension == null ) ? 0 : extension.hashCode() );
		result = prime * result + ( ( name == null ) ? 0 : name.hashCode() );
		result = prime * result + ( ( path == null ) ? 0 : path.hashCode() );
		return result;
	}

	@Override
	public boolean equals( Object obj ) {
		if ( this == obj )
			return true;
		if ( !Utils.getEntityClass( getClass() ).equals( Utils.getEntityClass( obj.getClass() ) ) )
			return false;
		Image other = (Image) obj;
		if ( getId() == null ) {
			if ( other.getId() != null )
				return false;
		} else if ( !getId().equals( other.getId() ) )
			return false;
		if ( getCreatedDate() == null ) {
			if ( other.getCreatedDate() != null )
				return false;
		} else if ( !getCreatedDate().equals( other.getCreatedDate() ) )
			return false;
		if ( getName() == null ) {
			if ( other.getName() != null )
				return false;
		} else if ( !getName().equals( other.getName() ) )
			return false;
		if ( getExtension() == null ) {
			if ( other.getExtension() != null )
				return false;
		} else if ( !getExtension().equals( other.getExtension() ) )
			return false;
		if ( getPath() == null ) {
			if ( other.getPath() != null )
				return false;
		} else if ( !getPath().equals( other.getPath() ) )
			return false;
		return true;
	}
}
