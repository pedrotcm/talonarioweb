package br.com.talonario.entities;

import java.io.Serializable;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

@Entity
@Table( name = "orgao" )
@XmlRootElement
public class Organ extends BaseEntity implements Serializable {

	private static final long serialVersionUID = -8988571332874864017L;

	@Id
	@GeneratedValue( strategy = GenerationType.IDENTITY )
	@Column( name = "pk_id_orgao" )
	private Integer id;

	@Column( name = "nome", nullable = false )
	private String name;

	@Column( name = "codigo_orgao", nullable = false )
	private Integer code;

	@ManyToMany( fetch = FetchType.LAZY )
	@JoinTable( name = "orgao_municipio", joinColumns = { @JoinColumn( name = "fk_id_orgao" ) }, inverseJoinColumns = { @JoinColumn( name = "fk_id_municipio" ) } )
	private Set<City> cities;

	@Column( name = "competencia" )
	private String competence;

	public Organ() {}

	@Override
	public Integer getId() {
		return id;
	}

	@Override
	public void setId( Integer id ) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName( String name ) {
		this.name = name;
	}

	public Integer getCode() {
		return code;
	}

	public void setCode( Integer code ) {
		this.code = code;
	}

	public Set<City> getCities() {
		return cities;
	}

	public void setCities( Set<City> cities ) {
		this.cities = cities;
	}

	public String getCompetence() {
		return competence;
	}

	public void setCompetence( String competence ) {
		this.competence = competence;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ( ( getCode() == null ) ? 0 : getCode().hashCode() );
		result = prime * result + ( ( getId() == null ) ? 0 : getId().hashCode() );
		result = prime * result + ( ( getName() == null ) ? 0 : getName().hashCode() );
		return result;
	}

	@Override
	public boolean equals( Object obj ) {
		if ( this == obj )
			return true;
		if ( !super.equals( obj ) )
			return false;
		if ( getClass() != obj.getClass() )
			return false;
		Organ other = (Organ) obj;
		if ( getCode() == null ) {
			if ( other.getCode() != null )
				return false;
		} else if ( !getCode().equals( other.getCode() ) )
			return false;
		if ( getId() == null ) {
			if ( other.getId() != null )
				return false;
		} else if ( !getId().equals( other.getId() ) )
			return false;
		if ( getName() == null ) {
			if ( other.getName() != null )
				return false;
		} else if ( !getName().equals( other.getName() ) )
			return false;
		return true;
	}

}