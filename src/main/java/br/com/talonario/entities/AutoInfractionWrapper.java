package br.com.talonario.entities;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class AutoInfractionWrapper implements Serializable {

	private static final long serialVersionUID = 517667170764707908L;

	private List<AutoInfraction> autoInfractions;

	public AutoInfractionWrapper() {
		autoInfractions = new ArrayList<AutoInfraction>();
	}

	public List<AutoInfraction> getAutoInfractions() {
		return autoInfractions;
	}

	public void setAutoInfractions( List<AutoInfraction> autoInfractions ) {
		this.autoInfractions = autoInfractions;
	}

}
