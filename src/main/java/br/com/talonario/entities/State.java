package br.com.talonario.entities;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

@Entity
@Table( name = "estado" )
@XmlRootElement
public class State extends BaseEntity implements Serializable {

	private static final long serialVersionUID = -6577793136404061993L;

	@Id
	@GeneratedValue( strategy = GenerationType.IDENTITY )
	@Column( name = "pk_id_estado" )
	private Integer id;

	@Column( name = "uf", length = 2, nullable = false )
	private String uf;

	@Column( name = "nome", length = 50, nullable = false )
	private String name;

	public State() {}

	@Override
	public Integer getId() {
		return id;
	}

	@Override
	public void setId( Integer id ) {
		this.id = id;
	}

	public String getUf() {
		return uf;
	}

	public void setUf( String uf ) {
		this.uf = uf;
	}

	public String getName() {
		return name;
	}

	public void setName( String name ) {
		this.name = name;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ( ( getId() == null ) ? 0 : getId().hashCode() );
		result = prime * result + ( ( getName() == null ) ? 0 : getName().hashCode() );
		result = prime * result + ( ( getUf() == null ) ? 0 : getUf().hashCode() );
		return result;
	}

	@Override
	public boolean equals( Object obj ) {
		if ( this == obj )
			return true;
		if ( !super.equals( obj ) )
			return false;
		if ( getClass() != obj.getClass() )
			return false;
		State other = (State) obj;
		if ( getId() == null ) {
			if ( other.getId() != null )
				return false;
		} else if ( !getId().equals( other.getId() ) )
			return false;
		if ( getName() == null ) {
			if ( other.getName() != null )
				return false;
		} else if ( !getName().equals( other.getName() ) )
			return false;
		if ( getUf() == null ) {
			if ( other.getUf() != null )
				return false;
		} else if ( !getUf().equals( other.getUf() ) )
			return false;
		return true;
	}

}
