package br.com.talonario.entities;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlRootElement;

@Entity
@Table( name = "pacote_numero_autos" )
@XmlRootElement
public class PackageNumberAutos extends BaseEntity implements Serializable {

	private static final long serialVersionUID = 1918180132403798521L;

	@Id
	@GeneratedValue( strategy = GenerationType.IDENTITY )
	@Column( name = "pk_id_pacote" )
	private Integer id;

	@Temporal( TemporalType.TIMESTAMP )
	@Column( name = "data_solicitacao" )
	private Date requestDate;

	@Column( name = "numero_inicial" )
	private Integer initNumber;

	@Column( name = "numero_final" )
	private Integer endNumber;

	@Column( name = "agente_username" )
	private String usernameAgent;

	@Column( name = "ultimo_numero" )
	private Integer currentNumber;

	@NotNull
	@ManyToOne( fetch = FetchType.LAZY )
	@JoinColumn( name = "fk_id_orgao" )
	private Organ organ;

	public PackageNumberAutos() {
		requestDate = new Date();
	}

	@Override
	public Integer getId() {
		return id;
	}

	@Override
	public void setId( Integer id ) {
		this.id = id;
	}

	public Date getRequestDate() {
		return requestDate;
	}

	public void setRequestDate( Date requestDate ) {
		this.requestDate = requestDate;
	}

	public Integer getInitNumber() {
		return initNumber;
	}

	public void setInitNumber( Integer initNumber ) {
		this.initNumber = initNumber;
	}

	public Integer getEndNumber() {
		return endNumber;
	}

	public void setEndNumber( Integer endNumber ) {
		this.endNumber = endNumber;
	}

	public String getUsernameAgent() {
		return usernameAgent;
	}

	public void setUsernameAgent( String usernameAgent ) {
		this.usernameAgent = usernameAgent;
	}

	public Organ getOrgan() {
		return organ;
	}

	public void setOrgan( Organ organ ) {
		this.organ = organ;
	}

	public Integer getCurrentNumber() {
		return currentNumber;
	}

	public void setCurrentNumber( Integer currentNumber ) {
		this.currentNumber = currentNumber;
	}

}
