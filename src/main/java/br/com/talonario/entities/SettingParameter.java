package br.com.talonario.entities;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@Entity
@Table( name = "parametros_configuracao" )
@XmlRootElement
public class SettingParameter extends BaseEntity implements Serializable {

	private static final long serialVersionUID = 6096218378325910844L;

	@XmlElement( name = "id_param_config" )
	@Id
	@GeneratedValue( strategy = GenerationType.IDENTITY )
	@Column( name = "pk_id_param_config" )
	private Integer id;

	@Column( name = "editar_data" )
	private Boolean isDateEdit;

	@Column( name = "quantidade_fotos" )
	private Integer qntPhotos;

	@Column( name = "validade_autos" )
	private Integer validityAutos;

	@Column( name = "quantidade_autos_pacote" )
	private Integer qntAutoInPackage;

	@Column( name = "minimo_autos_avisar" )
	private Integer minAutosWarn;

	@Column( name = "tempo_min_enviar_autos" )
	private Integer timeToSendAutos;

	@Column( name = "tempo_min_remover_autos" )
	private Integer timeToRemoveAutos;

	@Column( name = "caminho_imagem" )
	private String imagePath;

	@Column( name = "base_url_download" )
	private String baseDownloadUrl;

	public SettingParameter() {}

	@Override
	public Integer getId() {
		return id;
	}

	@Override
	public void setId( Integer id ) {
		this.id = id;
	}

	public Boolean getIsDateEdit() {
		return isDateEdit;
	}

	public void setIsDateEdit( Boolean isDateEdit ) {
		this.isDateEdit = isDateEdit;
	}

	public Integer getQntPhotos() {
		return qntPhotos;
	}

	public void setQntPhotos( Integer qntPhotos ) {
		this.qntPhotos = qntPhotos;
	}

	public Integer getValidityAutos() {
		return validityAutos;
	}

	public void setValidityAutos( Integer validityAutos ) {
		this.validityAutos = validityAutos;
	}

	public Integer getQntAutoInPackage() {
		return qntAutoInPackage;
	}

	public void setQntAutoInPackage( Integer qntAutoInPackage ) {
		this.qntAutoInPackage = qntAutoInPackage;
	}

	public Integer getMinAutosWarn() {
		return minAutosWarn;
	}

	public void setMinAutosWarn( Integer minAutosWarn ) {
		this.minAutosWarn = minAutosWarn;
	}

	public Integer getTimeToSendAutos() {
		return timeToSendAutos;
	}

	public void setTimeToSendAutos( Integer timeToSendAutos ) {
		this.timeToSendAutos = timeToSendAutos;
	}

	public Integer getTimeToRemoveAutos() {
		return timeToRemoveAutos;
	}

	public void setTimeToRemoveAutos( Integer timeToRemoveAutos ) {
		this.timeToRemoveAutos = timeToRemoveAutos;
	}

	public String getImagePath() {
		return imagePath;
	}

	public void setImagePath( String imagePath ) {
		this.imagePath = imagePath;
	}

	public String getBaseDownloadUrl() {
		return baseDownloadUrl;
	}

	public void setBaseDownloadUrl( String baseDownloadUrl ) {
		this.baseDownloadUrl = baseDownloadUrl;
	}

}
