package br.com.talonario.entities;

import java.util.Collection;
import java.util.Date;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;
import javax.xml.bind.annotation.XmlRootElement;

import org.springframework.security.core.userdetails.UserDetails;

@Entity
@Table( name = User.TABLE_NAME )
@XmlRootElement
public class User extends BaseEntity implements UserDetails {

	private static final long serialVersionUID = 8485404378247350057L;

	static final String TABLE_NAME = "usuario";

	@Id
	@GeneratedValue( strategy = GenerationType.IDENTITY )
	@Column( name = "pk_id_usuario" )
	private Integer id;

	@Column( name = "nome", length = 50 )
	private String name;

	@Column( nullable = false, unique = true )
	private String username;

	@Column( nullable = false )
	private String password;

	@Column( unique = true )
	private String token;

	@Temporal( TemporalType.DATE )
	@Column( name = "data_criacao" )
	private Date createdAt;

	@ManyToMany( fetch = FetchType.EAGER )
	@JoinTable( name = "usuario_orgao", joinColumns = { @JoinColumn( name = "fk_id_usuario" ) }, inverseJoinColumns = { @JoinColumn( name = "fk_id_orgao" ) } )
	private Set<Organ> organs;

	@ManyToMany( fetch = FetchType.EAGER )
	@JoinTable( name = "usuario_perfil", joinColumns = @JoinColumn( name = "fk_id_usuario" ), inverseJoinColumns = @JoinColumn( name = "fk_id_perfil" ) )
	private Set<Profile> profiles;

	private Boolean enabled;

	@Transient
	private String imei;

	public User() {
		this.enabled = true;
		this.createdAt = new Date();
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ( ( createdAt == null ) ? 0 : createdAt.hashCode() );
		result = prime * result + ( ( enabled == null ) ? 0 : enabled.hashCode() );
		result = prime * result + ( ( id == null ) ? 0 : id.hashCode() );
		result = prime * result + ( ( name == null ) ? 0 : name.hashCode() );
		result = prime * result + ( ( password == null ) ? 0 : password.hashCode() );
		result = prime * result + ( ( token == null ) ? 0 : token.hashCode() );
		result = prime * result + ( ( username == null ) ? 0 : username.hashCode() );
		return result;
	}

	@Override
	public boolean equals( Object obj ) {
		if ( this == obj )
			return true;
		if ( !super.equals( obj ) )
			return false;
		if ( getClass() != obj.getClass() )
			return false;
		User other = (User) obj;
		if ( createdAt == null ) {
			if ( other.createdAt != null )
				return false;
		} else if ( !createdAt.equals( other.createdAt ) )
			return false;
		if ( enabled == null ) {
			if ( other.enabled != null )
				return false;
		} else if ( !enabled.equals( other.enabled ) )
			return false;
		if ( id == null ) {
			if ( other.id != null )
				return false;
		} else if ( !id.equals( other.id ) )
			return false;
		if ( name == null ) {
			if ( other.name != null )
				return false;
		} else if ( !name.equals( other.name ) )
			return false;
		if ( password == null ) {
			if ( other.password != null )
				return false;
		} else if ( !password.equals( other.password ) )
			return false;
		if ( token == null ) {
			if ( other.token != null )
				return false;
		} else if ( !token.equals( other.token ) )
			return false;
		if ( username == null ) {
			if ( other.username != null )
				return false;
		} else if ( !username.equals( other.username ) )
			return false;
		return true;
	}

	@Override
	public Collection<Profile> getAuthorities() {
		return profiles;
	}

	@Override
	public String getPassword() {
		return password;
	}

	@Override
	public String getUsername() {
		return username;
	}

	@Override
	public boolean isAccountNonExpired() {
		return false;
	}

	@Override
	public boolean isAccountNonLocked() {
		return false;
	}

	@Override
	public boolean isCredentialsNonExpired() {
		return false;
	}

	@Override
	public boolean isEnabled() {
		return enabled;
	}

	public void setEnabled( Boolean enabled ) {
		this.enabled = enabled;
	}

	@Override
	public Integer getId() {
		return id;
	}

	@Override
	public void setId( Integer id ) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName( String name ) {
		this.name = name;
	}

	public String getToken() {
		return token;
	}

	public void setToken( String token ) {
		this.token = token;
	}

	public Date getCreatedAt() {
		return createdAt;
	}

	public void setCreatedAt( Date createdAt ) {
		this.createdAt = createdAt;
	}

	public Set<Organ> getOrgans() {
		return organs;
	}

	public void setOrgans( Set<Organ> organs ) {
		this.organs = organs;
	}

	public void setUsername( String username ) {
		this.username = username;
	}

	public void setPassword( String password ) {
		this.password = password;
	}

	public void setAuthorities( Set<Profile> profiles ) {
		this.profiles = profiles;
	}

	public String getImei() {
		return imei;
	}

	public void setImei( String imei ) {
		this.imei = imei;
	}

	public Set<Profile> getProfiles() {
		return (Set<Profile>) getAuthorities();
	}

	public void setProfiles( Set<Profile> profiles ) {
		this.profiles = profiles;
	}

}
