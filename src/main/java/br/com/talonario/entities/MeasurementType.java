package br.com.talonario.entities;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@Entity
@Table( name = "tipo_medicao" )
@XmlRootElement
public class MeasurementType extends BaseEntity implements Serializable {

	private static final long serialVersionUID = 5096153181907815053L;

	@XmlElement( name = "id_tipo_medicao" )
	@Id
	@GeneratedValue( strategy = GenerationType.IDENTITY )
	@Column( name = "pk_id_tipo_medicao" )
	private Integer id;

	@Column( name = "codigo" )
	private int code;

	@Column( name = "descricao" )
	private String description;

	@Column( name = "valor_permitido" )
	private String allowedValue;

	@Override
	public Integer getId() {
		return id;
	}

	@Override
	public void setId( Integer id ) {
		this.id = id;
	}

	public int getCode() {
		return code;
	}

	public void setCode( int code ) {
		this.code = code;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription( String description ) {
		this.description = description;
	}

	public String getAllowedValue() {
		return allowedValue;
	}

	public void setAllowedValue( String allowedValue ) {
		this.allowedValue = allowedValue;
	}

}
