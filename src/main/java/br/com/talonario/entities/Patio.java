package br.com.talonario.entities;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import jodd.util.StringUtil;

import org.apache.commons.lang3.StringUtils;

@Entity
@Table( name = "logradouro" )
@XmlRootElement
public class Patio extends BaseEntity implements Serializable {

	private static final long serialVersionUID = -1810137967695941025L;

	@XmlElement( name = "id_logradouro" )
	@Id
	@GeneratedValue( strategy = GenerationType.IDENTITY )
	@Column( name = "pk_id_logradouro" )
	private Integer id;

	@Column( name = "tipo_logradouro" )
	private String patioType;

	@Column( name = "descricao_logradouro" )
	private String patioDescription;

	@Column( name = "bairro" )
	private String neighborhood;

	public Patio() {}

	@Override
	public Integer getId() {
		return id;
	}

	@Override
	public void setId( Integer id ) {
		this.id = id;
	}

	public String getPatioType() {
		return patioType;
	}

	public void setPatioType( String patioType ) {
		this.patioType = patioType;
	}

	public String getPatioDescription() {
		return patioDescription;
	}

	public void setPatioDescription( String patioDescription ) {
		this.patioDescription = patioDescription;
	}

	public String getNeighborhood() {
		return neighborhood;
	}

	public void setNeighborhood( String neighborhood ) {
		this.neighborhood = neighborhood;
	}

}
