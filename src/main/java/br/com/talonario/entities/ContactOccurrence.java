package br.com.talonario.entities;

import java.io.Serializable;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;

@Entity
@Table( name = "contato_ocorrencia" )
@XmlRootElement
public class ContactOccurrence extends BaseEntity implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1797242242674307578L;

	@XmlElement( name = "id_contato_ocorrencia" )
	@Id
	@GeneratedValue( strategy = GenerationType.IDENTITY )
	@Column( name = "pk_id_contato_ocorrencia" )
	private Integer id;

	@Column( name = "nome", nullable = false )
	private String name;

	@Fetch( FetchMode.SUBSELECT )
	@OneToMany( orphanRemoval = true, cascade = { CascadeType.ALL }, fetch = FetchType.EAGER )
	@JoinTable( name = "contato_telefone_ocorrencia", joinColumns = @JoinColumn( name = "fk_id_contato" ), inverseJoinColumns = @JoinColumn( name = "fk_id_telefone" ) )
	private Set<TelephoneOccurrence> telephones;

	@Override
	public Integer getId() {
		return id;
	}

	@Override
	public void setId( Integer id ) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName( String name ) {
		this.name = name;
	}

	public Set<TelephoneOccurrence> getTelephones() {
		return telephones;
	}

	public void setTelephones( Set<TelephoneOccurrence> telephones ) {
		this.telephones = telephones;
	}

}
