package br.com.talonario.entities;

import java.io.Serializable;

public class CustomResponseEntity implements Serializable {

	private static final long serialVersionUID = -8934186531842680809L;

	private Integer code;
	private String message;
	private Object object;

	public CustomResponseEntity() {}

	public CustomResponseEntity( Integer code ) {
		this.code = code;
	}

	public CustomResponseEntity( Integer code, String message ) {
		this.code = code;
		this.message = message;
	}

	public CustomResponseEntity( Integer code, Object object ) {
		this.code = code;
		this.object = object;
	}

	public CustomResponseEntity( Integer code, String message, Object object ) {
		this.code = code;
		this.message = message;
		this.object = object;
	}

	public Integer getCode() {
		return code;
	}

	public void setCode( Integer code ) {
		this.code = code;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage( String message ) {
		this.message = message;
	}

	public Object getObject() {
		return object;
	}

	public void setObject( Object object ) {
		this.object = object;
	}

}
