package br.com.talonario.entities;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@Entity
@Table( name = "especie" )
@XmlRootElement
public class Specie extends BaseEntity implements Serializable {

	private static final long serialVersionUID = -4765392553403448843L;

	@XmlElement( name = "id_especie" )
	@Id
	@GeneratedValue( strategy = GenerationType.IDENTITY )
	@Column( name = "pk_id_especie" )
	private Integer id;

	@Column( name = "descricao" )
	private String description;

	@Column( name = "ordem" )
	private Integer order;

	public Specie() {}

	@Override
	public Integer getId() {
		return id;
	}

	@Override
	public void setId( Integer id ) {
		this.id = id;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription( String description ) {
		this.description = description;
	}

	public Integer getOrder() {
		return order;
	}

	public void setOrder( Integer order ) {
		this.order = order;
	}

}
