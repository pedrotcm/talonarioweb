package br.com.talonario.controllers;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.event.ValueChangeEvent;
import javax.inject.Inject;
import javax.inject.Named;

import org.springframework.context.annotation.Scope;

import br.com.talonario.entities.City;
import br.com.talonario.entities.Organ;
import br.com.talonario.entities.State;
import br.com.talonario.services.CityService;
import br.com.talonario.services.OrganService;

@Named
@Scope( "view" )
public class OrganController implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 7097543293623832716L;

	private Organ entity;
	private State stateEntity;
	private Set<City> availableCities;

	@Inject
	private OrganService organService;

	@Inject
	private CityService cityService;

	@PostConstruct
	public void init() {
		clean();
	}

	public void clean() {
		this.entity = new Organ();
		this.entity.setCities( new HashSet<City>() );
		this.availableCities = new HashSet<City>();
		this.stateEntity = new State();
	}

	public void create() {
		Organ loadedOrgan = organService.findOrganByNameAndCode( entity.getName(), entity.getCode() );

		if ( loadedOrgan == null ) {
			organService.save( entity );
		} else {
			FacesContext.getCurrentInstance().addMessage( null, new FacesMessage( FacesMessage.SEVERITY_ERROR, "Erro!", "Já existe um orgão " + entity.getName() + " cadastrado." ) );
			return;
		}

		FacesContext.getCurrentInstance().addMessage( null, new FacesMessage( "Operação realizada com sucesso" ) );
		clean();
	}

	public void selectAll() {

	}

	public void changeState( ValueChangeEvent event ) {
		if ( event != null && event.getNewValue() != null )
			this.stateEntity = (State) event.getNewValue();
		this.availableCities = cityService.findByStateOrderByNameAsc( stateEntity );
	}

	public Organ getEntity() {
		return entity;
	}

	public void setEntity( Organ entity ) {
		this.entity = entity;
	}

	public State getStateEntity() {
		return stateEntity;
	}

	public void setStateEntity( State stateEntity ) {
		this.stateEntity = stateEntity;
	}

	public Set<City> getAvailableCities() {
		return availableCities;
	}

	public void setAvailableCities( Set<City> availableCities ) {
		this.availableCities = availableCities;
	}

}