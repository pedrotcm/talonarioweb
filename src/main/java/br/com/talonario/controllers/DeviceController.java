package br.com.talonario.controllers;

import java.io.Serializable;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.inject.Named;

import org.springframework.context.annotation.Scope;

import br.com.talonario.entities.Device;
import br.com.talonario.services.DeviceService;

@Named
@Scope( "view" )
public class DeviceController implements Serializable {

	private static final long serialVersionUID = 5297763926422961707L;

	private Device entity;

	@Inject
	private DeviceService deviceService;

	@PostConstruct
	public void init() {
		clean();
	}

	public void clean() {
		entity = new Device();
	}

	public void create() {
		Device loadedDevice = deviceService.findDeviceByImei( entity.getImei() );
		if ( loadedDevice == null )
			deviceService.save( entity );
		else {
			FacesContext.getCurrentInstance().addMessage( null, new FacesMessage( FacesMessage.SEVERITY_ERROR, "Erro!", "Dispositivo já existe." ) );
			return;
		}

		FacesContext.getCurrentInstance().addMessage( null, new FacesMessage( "Operação realizada com sucesso" ) );
		clean();
	}

	public Device getEntity() {
		return entity;
	}

	public void setEntity( Device entity ) {
		this.entity = entity;
	}

}
