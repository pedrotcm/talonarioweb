package br.com.talonario.controllers;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;

import org.primefaces.component.datatable.DataTable;
import org.primefaces.component.tabview.Tab;
import org.primefaces.component.tabview.TabView;
import org.primefaces.context.RequestContext;
import org.primefaces.event.TabCloseEvent;
import org.primefaces.model.LazyDataModel;
import org.primefaces.model.SortOrder;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.data.domain.Sort.Order;

import br.com.talonario.utils.LazySorter;
import br.com.talonario.utils.ResourceBundleUtils;

public abstract class BaseSearchController<T> implements Serializable {
	private static final long serialVersionUID = -1236184169177912409L;

	private T searchEntity;

	private T editEntity;

	private T showEntity;

	private T removeEntity;

	private Class<T> searchEntityClass;

	private LazyDataModel<T> dataModel;

	private Tab showTab;

	private Tab searchTab;

	private Tab editTab;

	private Tab cancelTab;

	private DataTable searchResults;

	public BaseSearchController( Class<T> searchEntityClass ) {
		this.searchEntityClass = searchEntityClass;
		newSearchEntity();
	}

	public void newSearchEntity() {
		try {
			searchEntity = searchEntityClass.newInstance();
		} catch ( Exception e ) {
			// FIXME: handle exception
		}
	}

	public T getSearchEntity() {
		return searchEntity;
	}

	public void setSearchEntity( T searchEntity ) {
		this.searchEntity = searchEntity;
	}

	public T getEditEntity() {
		return editEntity;
	}

	public void setEditEntity( T editEntity ) {
		this.editEntity = editEntity;
	}

	public T getShowEntity() {
		return showEntity;
	}

	public void setShowEntity( T showEntity ) {
		this.showEntity = showEntity;
	}

	public T getRemoveEntity() {
		return removeEntity;
	}

	public void setRemoveEntity( T removeEntity ) {
		this.removeEntity = removeEntity;
	}

	public DataTable getSearchResults() {
		return this.searchResults;
	}

	private void setUpDataTable( DataTable table ) {
		table.setLazy( true );
		table.setPaginator( true );
		table.setPaginatorPosition( "bottom" );
		table.setEmptyMessage( ResourceBundleUtils.getLocalizedMessage( "table.emptyMessage" ) );
		table.setRows( Integer.parseInt( ResourceBundleUtils.getLocalizedMessage( "table.rows" ) ) );
		table.setVar( "item" );
		table.setRowsPerPageTemplate( ResourceBundleUtils.getLocalizedMessage( "table.rowsPerPageTemplate" ) );
		table.setPaginatorTemplate( ResourceBundleUtils.getLocalizedMessage( "table.paginatorTemplate" ) );
		table.setCurrentPageReportTemplate( ResourceBundleUtils.getLocalizedMessage( "table.currentPageReportTemplate" ) );
	}

	public void onTabClose( TabCloseEvent event ) {
		closeTab( event.getTab() );
	}

	public void closeTab( Tab tab ) {
		TabView parent = (TabView) tab.getParent();
		tab.setRendered( false );
		parent.setActiveIndex( 0 );
		complementOnTabClose( tab );
	}

	public void complementOnTabClose( Tab tab ) {
		try {
			if ( tab.equals( getShowTab() ) ) {
				showEntity = searchEntityClass.newInstance();
			}

			if ( tab.equals( getEditTab() ) ) {
				editEntity = searchEntityClass.newInstance();
				RequestContext.getCurrentInstance().reset( ":" + tab.getParent().getId() + ":editForm" );
			}
		} catch ( InstantiationException e ) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch ( IllegalAccessException e ) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public void setSearchResults( DataTable searchResults ) {
		this.searchResults = searchResults;
		setUpDataTable( getSearchResults() );
	}

	public LazyDataModel<T> getDataModel() {
		return dataModel;
	}

	public void setDataModel( LazyDataModel<T> dataModel ) {
		this.dataModel = dataModel;
	}

	public void find() {
		loadModel();
		DataTable results = this.getSearchResults();
		if ( results != null )
			results.setFirst( 0 );
	}

	private void loadModel() {
		setDataModel( new LazyDataModel<T>() {

			private static final long serialVersionUID = -7141739005508115863L;

			@SuppressWarnings( { "unchecked", "rawtypes" } )
			@Override
			public List<T> load( int first, int pageSize, String sortField, SortOrder sortOrder, Map<String, Object> filters ) {

				Sort sort = null;
				if ( sortField != null && !sortField.isEmpty() ) {
					sort = new Sort( new Order( sortOrder == SortOrder.DESCENDING ? Direction.DESC : Direction.ASC, sortField ) );
				}

				Page<T> results = getList( sort );
				this.setRowCount( (int) results.getTotalElements() );

				// sort
				List<T> resultsList = new ArrayList<T>( results.getContent() );
				if ( sortField != null ) {
					Collections.sort( resultsList, new LazySorter( sortField, sortOrder ) );
				}

				// rowCount
				int dataSize = (int) results.getTotalElements();
				this.setRowCount( dataSize );

				// paginate
				if ( dataSize > pageSize ) {
					try {
						return resultsList.subList( first, first + pageSize );
					} catch ( IndexOutOfBoundsException e ) {
						return resultsList.subList( first, first + ( dataSize % pageSize ) );
					}
				} else {
					return resultsList;
				}
			}
			//
			// @Override
			// public void setRowIndex( int rowIndex ) {
			// /*
			// * The following is in ancestor (LazyDataModel): this.rowIndex =
			// * rowIndex == -1 ? rowIndex : (rowIndex % pageSize);
			// */
			// if ( rowIndex == -1 || getPageSize() == 0 ) {
			// super.setRowIndex( -1 );
			// } else
			// super.setRowIndex( rowIndex % getPageSize() );
			// }

		} );
	}

	public void edit() {
		TabView parent = (TabView) getEditTab().getParent();
		parent.setActiveIndex( 1 );
		getEditTab().setRendered( true );
	}

	public void show() {
		TabView parent = (TabView) getShowTab().getParent();
		parent.setActiveIndex( verificarIndex( getShowTab().getId() ) );
		getShowTab().setRendered( true );
	}

	public void loadToCancel() {
		TabView parent = (TabView) getCancelTab().getParent();
		parent.setActiveIndex( verificarIndex( getCancelTab().getId() ) );
		getCancelTab().setRendered( true );
	}

	private int verificarIndex( String operacao ) {
		if ( operacao != null ) {
			if ( getEditTab() != null && getCancelTab() != null && getShowTab() != null ) {
				return verificarOrdemTresOperacoes( operacao );
			} else {
				return verificarOrdemDuasOperacoes( operacao );
			}
		} else {
			return 0;
		}
	}

	private int verificarOrdemTresOperacoes( String operacao ) {
		if ( operacao.equals( "cancelTab" ) ) {
			if ( ( getEditTab().isRendered() && getCancelTab().isRendered() && getShowTab().isRendered() ) || ( getCancelTab().isRendered() && getEditTab().isRendered() ) || ( getEditTab().isRendered() ) ) {
				return 2;
			} else {
				return 1;
			}
		}

		if ( operacao.equals( "showTab" ) ) {
			if ( ( getEditTab().isRendered() && getCancelTab().isRendered() && getShowTab().isRendered() ) || ( getCancelTab().isRendered() && getEditTab().isRendered() ) ) {
				return 3;
			} else if ( getCancelTab().isRendered() || getEditTab().isRendered() ) {
				return 2;
			} else {
				return 1;
			}
		}
		return 1;
	}

	private int verificarOrdemDuasOperacoes( String operacao ) {
		if ( operacao.equals( "cancelTab" ) ) {
			return 1;
		}

		if ( ( getCancelTab() != null && getCancelTab().isRendered() ) || ( getShowTab() != null && getShowTab().isRendered() ) ) {
			return 2;
		} else {
			return 1;
		}
	}

	public Tab getShowTab() {
		return showTab;
	}

	public void setShowTab( Tab showTab ) {
		this.showTab = showTab;
	}

	public Tab getSearchTab() {
		return searchTab;
	}

	public void setSearchTab( Tab searchTab ) {
		this.searchTab = searchTab;
	}

	public Tab getEditTab() {
		return editTab;
	}

	public void setEditTab( Tab editTab ) {
		this.editTab = editTab;
	}

	public Tab getCancelTab() {
		return cancelTab;
	}

	public void setCancelTab( Tab cancelTab ) {
		this.cancelTab = cancelTab;
	}

	public Class<T> getSearchEntityClass() {
		return searchEntityClass;
	}

	public void setSearchEntityClass( Class<T> searchEntityClass ) {
		this.searchEntityClass = searchEntityClass;
	}

	protected abstract Page<T> getList( Sort sort );

}
