package br.com.talonario.controllers;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.inject.Named;

import org.springframework.context.annotation.Scope;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Sort;

import br.com.talonario.entities.ContactOccurrence;
import br.com.talonario.entities.TelephoneOccurrence;
import br.com.talonario.services.ContactOccurrenceService;

@Named
@Scope( "view" )
public class ContactOccurrenceManageController extends BaseSearchController<ContactOccurrence> implements Serializable {

	private static final long serialVersionUID = 2881576808376953180L;

	@Inject
	private ContactOccurrenceService contactOccurrenceService;

	private ContactOccurrence removeEntity;

	private List<TelephoneOccurrence> telephones;
	private TelephoneOccurrence telephone;
	private TelephoneOccurrence standardTelephone;

	public ContactOccurrenceManageController() {
		super( ContactOccurrence.class );
	}

	@PostConstruct
	public void init() {
		this.setEditEntity( new ContactOccurrence() );
		this.setRemoveEntity( new ContactOccurrence() );
		this.setSearchEntity( new ContactOccurrence() );
		this.telephone = new TelephoneOccurrence();
	}

	public void clean() {
		this.setSearchEntity( new ContactOccurrence() );
	}

	@Override
	protected Page<ContactOccurrence> getList( Sort sort ) {
		return new PageImpl<ContactOccurrence>( contactOccurrenceService.find( getSearchEntity(), sort ) );
	}

	@Override
	public void edit() {
		super.edit();
		this.telephones = new ArrayList<TelephoneOccurrence>( getEditEntity().getTelephones() );
		for ( TelephoneOccurrence telephone : this.telephones ) {
			if ( telephone.getStandard() ) {
				this.standardTelephone = telephone;
				telephone.setStandard( false );
			}
		}
	}

	public void cancel() {
		closeTab( getEditTab() );
	}

	public void update() {
		if ( telephones.isEmpty() ) {
			FacesContext.getCurrentInstance().addMessage( null, new FacesMessage( FacesMessage.SEVERITY_ERROR, "Telefone é obrigatório", "Deve ser adicionado pelo menos um telefone." ) );
			return;
		}

		ContactOccurrence contactLoaded = contactOccurrenceService.findContactOccurrenceByName( getEditEntity().getName() );
		if ( contactLoaded != null && !getEditEntity().getName().equalsIgnoreCase( contactLoaded.getName() ) ) {
			FacesContext.getCurrentInstance().addMessage( null, new FacesMessage( FacesMessage.SEVERITY_ERROR, "Erro!", "Nome do contato já existe." ) );
			return;
		}

		if ( standardTelephone == null ) {
			FacesContext.getCurrentInstance().addMessage( null, new FacesMessage( FacesMessage.SEVERITY_ERROR, "Erro!", "Deve ser selecionado um telefone como padrão." ) );
			return;
		}

		int index = telephones.indexOf( standardTelephone );
		telephones.get( index ).setStandard( true );

		getEditEntity().getTelephones().clear();
		getEditEntity().getTelephones().addAll( telephones );
		contactOccurrenceService.save( getEditEntity() );

		FacesContext.getCurrentInstance().addMessage( null, new FacesMessage( "Operação realizada com sucesso" ) );
		closeTab( getEditTab() );
	}

	public void remove() {
		contactOccurrenceService.delete( removeEntity );
		FacesContext.getCurrentInstance().addMessage( null, new FacesMessage( "Operação realizada com sucesso" ) );
	}

	public void removeTelephone() {
		if ( telephones == null ) {
			telephones = new ArrayList<TelephoneOccurrence>();
			return;
		}

		if ( standardTelephone != null && telephone.equals( standardTelephone ) )
			standardTelephone = new TelephoneOccurrence();

		telephones.remove( telephone );

		if ( telephones.isEmpty() ) {
			telephones = new ArrayList<TelephoneOccurrence>();
			telephone = new TelephoneOccurrence();
			standardTelephone = new TelephoneOccurrence();
		}
	}

	public void addTelephone() {
		if ( telephones == null )
			telephones = new ArrayList<TelephoneOccurrence>();

		if ( telephone.getTelephone() == null || telephone.getTelephone().isEmpty() ) {
			FacesContext.getCurrentInstance().addMessage( null, new FacesMessage( FacesMessage.SEVERITY_ERROR, "Telefone é obrigatório", "O campo telefone deve ser preenchido." ) );
			return;
		}

		if ( !telephones.contains( telephone ) ) {
			telephones.add( telephone );
		}

		telephone = new TelephoneOccurrence();

	}

	@Override
	public ContactOccurrence getRemoveEntity() {
		return removeEntity;
	}

	@Override
	public void setRemoveEntity( ContactOccurrence removeEntity ) {
		this.removeEntity = removeEntity;
	}

	public List<TelephoneOccurrence> getTelephones() {
		return telephones;
	}

	public void setTelephones( List<TelephoneOccurrence> telephones ) {
		this.telephones = telephones;
	}

	public TelephoneOccurrence getTelephone() {
		return telephone;
	}

	public void setTelephone( TelephoneOccurrence telephone ) {
		this.telephone = telephone;
	}

	public TelephoneOccurrence getStandardTelephone() {
		return standardTelephone;
	}

	public void setStandardTelephone( TelephoneOccurrence standardTelephone ) {
		this.standardTelephone = standardTelephone;
	}

}
