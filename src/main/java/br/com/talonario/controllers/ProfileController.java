package br.com.talonario.controllers;

import java.io.Serializable;
import java.util.List;

import javax.inject.Inject;
import javax.inject.Named;

import org.springframework.context.annotation.Scope;

import br.com.talonario.entities.Profile;
import br.com.talonario.services.ProfileService;

@Named
@Scope( "view" )
public class ProfileController implements Serializable {

	private static final long serialVersionUID = 1464032359879128016L;

	@Inject
	private ProfileService profileService;

	public List<Profile> autocompleteProfilesNotAdmin() {
		return profileService.findByDescriptionNot( "Administrador" );
	}

}
