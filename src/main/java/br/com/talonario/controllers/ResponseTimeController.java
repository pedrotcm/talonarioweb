package br.com.talonario.controllers;

import java.io.Serializable;

import javax.inject.Named;

import org.springframework.context.annotation.Scope;
import org.springframework.security.core.context.SecurityContextHolder;

@Named
@Scope( "session" )
public class ResponseTimeController implements Serializable {

	private static final long serialVersionUID = -8273271016908447630L;

	public void logout() {
		SecurityContextHolder.getContext().setAuthentication( null );
	}

}
