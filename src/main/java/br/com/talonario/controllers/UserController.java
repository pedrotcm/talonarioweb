package br.com.talonario.controllers;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.inject.Named;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;

import br.com.talonario.entities.Organ;
import br.com.talonario.entities.Profile;
import br.com.talonario.entities.User;
import br.com.talonario.enums.ProfileEnum;
import br.com.talonario.repositories.OrganRepository;
import br.com.talonario.services.impl.UserServiceImpl;

@Named
@Scope( "view" )
public class UserController implements Serializable {

	private static final long serialVersionUID = 7097543293623832716L;

	private User entity;
	private String confirmPassword;
	private List<Organ> organs;
	private Profile profile;

	@Autowired
	private UserServiceImpl userService;

	@Autowired
	private OrganRepository organRepository;

	@PostConstruct
	public void init() {
		organs = new ArrayList<Organ>();
		organs = organRepository.findAll();
		clean();
	}

	public void clean() {
		this.entity = new User();
		this.confirmPassword = null;
		this.profile = null;
	}

	public void create() {
		User loadedUser = userService.findUserByUsername( entity.getUsername() );

		if ( entity.getPassword().length() < 8 || confirmPassword.length() < 8 ) {
			FacesContext.getCurrentInstance().addMessage( null, new FacesMessage( FacesMessage.SEVERITY_ERROR, "Erro!", "A senha deve ter no mínimo 8 caracteres." ) );
			return;
		} else if ( !entity.getPassword().equals( confirmPassword ) ) {
			FacesContext.getCurrentInstance().addMessage( null, new FacesMessage( FacesMessage.SEVERITY_ERROR, "Erro!", "Confirmar senha diferente da senha." ) );
			return;
		}

		if ( loadedUser == null ) {
			Set<Profile> selectedProfile = new HashSet<Profile>();
			selectedProfile.add( profile );
			entity.setAuthorities( selectedProfile );
			userService.create( entity );
		} else {
			FacesContext.getCurrentInstance().addMessage( null, new FacesMessage( FacesMessage.SEVERITY_ERROR, "Erro!", "Nome de usuário já existe." ) );
			return;
		}

		FacesContext.getCurrentInstance().addMessage( null, new FacesMessage( "Operação realizada com sucesso" ) );
		clean();
	}

	public void cleanProfile() {
		if ( !profile.getDescription().equalsIgnoreCase( "APP" ) )
			profile = null;
	}

	public Boolean checkProfileAPP() {
		if ( profile != null && profile.getAuthority().equals( ProfileEnum.APP.toString() ) )
			return true;
		return false;
	}

	public User getEntity() {
		return entity;
	}

	public void setEntity( User entity ) {
		this.entity = entity;
	}

	public String getConfirmPassword() {
		return confirmPassword;
	}

	public void setConfirmPassword( String confirmPassword ) {
		this.confirmPassword = confirmPassword;
	}

	public List<Organ> getOrgans() {
		return organs;
	}

	public void setOrgans( List<Organ> organs ) {
		this.organs = organs;
	}

	public Profile getProfile() {
		return profile;
	}

	public void setProfile( Profile profile ) {
		this.profile = profile;
	}

}
