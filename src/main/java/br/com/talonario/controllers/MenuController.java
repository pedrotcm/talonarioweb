package br.com.talonario.controllers;

import java.io.InputStream;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.annotation.PostConstruct;
import javax.faces.component.UIComponent;
import javax.faces.component.UIComponentBase;
import javax.inject.Inject;
import javax.inject.Named;

import org.primefaces.component.menuitem.UIMenuItem;
import org.primefaces.component.submenu.UISubmenu;
import org.primefaces.model.menu.MenuElement;
import org.primefaces.model.menu.MenuModel;
import org.springframework.context.annotation.Scope;
import org.yaml.snakeyaml.Yaml;
import org.yaml.snakeyaml.constructor.CustomClassLoaderConstructor;

import br.com.talonario.entities.Profile;
import br.com.talonario.entities.User;
import br.com.talonario.helpers.MenuTreeNode;
import br.com.talonario.helpers.MyDefaultMenuModel;
import br.com.talonario.services.UserService;

@Named
@Scope( "view" )
public class MenuController implements Serializable {

	private static final long serialVersionUID = -5276868254078531270L;

	private MenuModel model;
	private List<MenuTreeNode> menuItems;

	@Inject
	private UserService userService;

	private Set<String> authorizedMenuItems;

	@PostConstruct
	public void init() {
		buildMenu();
	}

	public void buildMenu() {
		ClassLoader classLoader = Thread.currentThread().getContextClassLoader();

		InputStream resource = classLoader.getResourceAsStream( "menu.yaml" );
		Yaml yaml = new Yaml( new CustomClassLoaderConstructor( MenuTreeNode.class, classLoader ) );
		MenuTreeNode rootMenu = (MenuTreeNode) yaml.load( resource );
		menuItems = menuLeafs( rootMenu );

		User user = userService.loadCurrentUser();

		authorizedMenuItems = new HashSet<String>();

		for ( Profile profile : user.getAuthorities() ) {
			if ( profile != null && profile.getMenuItems() != null && !profile.getMenuItems().isEmpty() )
				authorizedMenuItems.addAll( profile.getMenuItems() );
		}

		authorizedMenuItems.add( "EXIT" );

		UISubmenu rootSubmenu = new UISubmenu();

		buildMenuNodes( rootMenu, rootSubmenu );

		rootSubmenu = (UISubmenu) rootSubmenu.getChildren().get( 0 );

		model = new MyDefaultMenuModel();
		// model = new DefaultMenuModel();
		for ( UIComponent child : rootSubmenu.getChildren() ) {
			child.setTransient( true );

			if ( child.getChildCount() > 0 ) {
				model.addElement( (MenuElement) child );
			} else {
				model.addElement( (MenuElement) child );
			}

		}
	}

	private List<MenuTreeNode> menuLeafs( MenuTreeNode rootMenu ) {
		ArrayList<MenuTreeNode> list = new ArrayList<MenuTreeNode>();
		for ( MenuTreeNode item : rootMenu.getChildren() ) {
			List<MenuTreeNode> children = item.getChildren();
			MenuTreeNode newItem = new MenuTreeNode();
			newItem.setCode( item.getCode() );
			newItem.setUrl( item.getUrl() );
			newItem.setIcon( item.getIcon() );
			if ( rootMenu.getDescription() != null )
				newItem.setDescription( rootMenu.getDescription() + " > " + item.getDescription() );
			else
				newItem.setDescription( item.getDescription() );
			if ( children == null || children.isEmpty() ) {
				if ( item.getCode() != null && !item.getCode().equals( "EXIT" ) )
					list.add( newItem );
			} else {
				list.addAll( menuLeafs( item ) );
			}
		}
		return list;
	}

	private void buildMenuNodes( MenuTreeNode node, UIComponentBase root ) {
		UIComponentBase processed = process( node, root );
		if ( processed != null && node.getChildren() != null ) {
			for ( MenuTreeNode n : node.getChildren() ) {
				buildMenuNodes( n, processed );
			}
		}
	}

	private UIComponentBase process( MenuTreeNode node, UIComponent parent ) {
		if ( node.getChildren() != null ) {
			if ( !hasAuthorizedChild( node ) )
				return null;
			UISubmenu submenu = new UISubmenu();
			submenu.setLabel( node.getDescription() );
			submenu.setIcon( node.getIcon() );
			parent.getChildren().add( submenu );
			return submenu;
		} else {
			if ( !authorizedMenuItems.contains( node.getCode() ) )
				return null;
			UIMenuItem menuItem = new UIMenuItem();
			menuItem.setValue( node.getDescription() );
			menuItem.setUrl( node.getUrl() );
			menuItem.setIcon( node.getIcon() );
			parent.getChildren().add( menuItem );
			return menuItem;
		}
	}

	private boolean hasAuthorizedChild( MenuTreeNode node ) {
		for ( MenuTreeNode child : node.getChildren() ) {
			if ( ( child.getCode() != null && authorizedMenuItems.contains( child.getCode() ) ) || ( child.getChildren() != null && hasAuthorizedChild( child ) ) )
				return true;
		}
		return false;
	}

	public MenuModel getModel() {
		return model;
	}

	public List<MenuTreeNode> getMenuItems() {
		return menuItems;
	}
}