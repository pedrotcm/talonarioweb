package br.com.talonario.controllers;

import java.io.Serializable;
import java.util.List;

import javax.inject.Named;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.data.domain.Sort.Order;

import br.com.talonario.entities.State;
import br.com.talonario.repositories.StateRepository;

@Named
@Scope( "view" )
public class StateController implements Serializable {

	private static final long serialVersionUID = 2012343248141760925L;

	@Autowired
	private StateRepository stateRepository;

	public List<State> autocomplete( String query ) {
		query = query.trim();
		List<State> states = stateRepository.findByUfContainingIgnoreCase( query, new Sort( new Order( Direction.ASC, "uf" ) ) );
		return states;
	}

}
