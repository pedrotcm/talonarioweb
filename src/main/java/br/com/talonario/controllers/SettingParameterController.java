package br.com.talonario.controllers;

import java.io.Serializable;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.inject.Named;

import org.springframework.context.annotation.Scope;

import br.com.talonario.entities.SettingParameter;
import br.com.talonario.services.SettingParameterService;

@Named
@Scope( "view" )
public class SettingParameterController implements Serializable {

	private static final long serialVersionUID = 6849316758430382781L;

	@Inject
	private SettingParameterService parameterService;

	private SettingParameter entity;

	@PostConstruct
	public void init() {
		clean();
		List<SettingParameter> parameters = parameterService.findAll();
		if ( parameters != null && !parameters.isEmpty() )
			entity = parameters.get( 0 );
	}

	public void create() {
		parameterService.save( entity );
		FacesContext.getCurrentInstance().addMessage( null, new FacesMessage( "Operação realizada com sucesso" ) );
		entity = parameterService.findOne( entity.getId() );
	}

	public void clean() {
		entity = new SettingParameter();
	}

	public SettingParameter getEntity() {
		return entity;
	}

	public void setEntity( SettingParameter entity ) {
		this.entity = entity;
	}

}
