package br.com.talonario.controllers;

import java.io.Serializable;

import javax.inject.Named;

import org.primefaces.component.datatable.DataTable;
import org.primefaces.component.tabview.Tab;
import org.springframework.context.annotation.Scope;

@Named
@Scope( "request" )
public class TabViewController implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -7965120517418625242L;

	private Tab showTab;

	private Tab searchTab;

	private Tab editTab;

	private Tab cancelTab;

	private DataTable searchResults;

	public Tab getShowTab() {
		return showTab;
	}

	public void setShowTab( Tab showTab ) {
		this.showTab = showTab;
	}

	public Tab getEditTab() {
		return editTab;
	}

	public void setEditTab( Tab editTab ) {
		this.editTab = editTab;
	}

	public Tab getCancelTab() {
		return cancelTab;
	}

	public void setCancelTab( Tab cancelTab ) {
		this.cancelTab = cancelTab;
	}

	public Tab getSearchTab() {
		return searchTab;
	}

	public void setSearchTab( Tab searchTab ) {
		this.searchTab = searchTab;
	}

	public DataTable getSearchResults() {
		return searchResults;
	}

	public void setSearchResults( DataTable searchResults ) {
		this.searchResults = searchResults;
	}

}
