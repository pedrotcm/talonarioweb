package br.com.talonario.controllers;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.inject.Named;

import org.springframework.context.annotation.Scope;

import br.com.talonario.entities.ContactOccurrence;
import br.com.talonario.entities.TelephoneOccurrence;
import br.com.talonario.services.ContactOccurrenceService;

@Named
@Scope( "view" )
public class ContactOccurrenceController implements Serializable {

	private static final long serialVersionUID = -3328659047105583943L;

	private ContactOccurrence entity;
	private List<TelephoneOccurrence> telephones;
	private TelephoneOccurrence telephone;
	private TelephoneOccurrence standardTelephone;

	@Inject
	private ContactOccurrenceService contactOccurenceService;

	@PostConstruct
	public void init() {
		clean();
	}

	public void clean() {
		entity = new ContactOccurrence();
		telephones = new ArrayList<TelephoneOccurrence>();
		telephone = new TelephoneOccurrence();
		standardTelephone = new TelephoneOccurrence();
	}

	public void create() {
		if ( entity.getTelephones() == null )
			entity.setTelephones( new HashSet<TelephoneOccurrence>() );
		if ( telephones.isEmpty() ) {
			FacesContext.getCurrentInstance().addMessage( null, new FacesMessage( FacesMessage.SEVERITY_ERROR, "Telefone é obrigatório", "Deve ser adicionado pelo menos um telefone." ) );
			return;
		}
		if ( contactOccurenceService.findContactOccurrenceByNameIgnoreCase( entity.getName() ) != null ) {
			FacesContext.getCurrentInstance().addMessage( null, new FacesMessage( FacesMessage.SEVERITY_ERROR, "Erro!", "Nome do contato já existe." ) );
			return;
		}

		if ( standardTelephone == null ) {
			FacesContext.getCurrentInstance().addMessage( null, new FacesMessage( FacesMessage.SEVERITY_ERROR, "Erro!", "Deve ser selecionado um telefone como padrão." ) );
			return;
		}

		int index = telephones.indexOf( standardTelephone );
		telephones.get( index ).setStandard( true );

		entity.getTelephones().addAll( telephones );
		contactOccurenceService.save( entity );

		FacesContext.getCurrentInstance().addMessage( null, new FacesMessage( "Operação realizada com sucesso" ) );
		clean();
	}

	public void addTelephone() {
		if ( telephones == null )
			telephones = new ArrayList<TelephoneOccurrence>();

		if ( telephone.getTelephone() == null || telephone.getTelephone().isEmpty() ) {
			FacesContext.getCurrentInstance().addMessage( null, new FacesMessage( FacesMessage.SEVERITY_ERROR, "Telefone é obrigatório", "O campo telefone deve ser preenchido." ) );
			return;
		}

		if ( !telephones.contains( telephone ) )
			telephones.add( telephone );

		telephone = new TelephoneOccurrence();

	}

	public void removeTelephone() {
		if ( telephones == null ) {
			telephones = new ArrayList<TelephoneOccurrence>();
			return;
		}

		if ( standardTelephone != null && telephone.equals( standardTelephone ) )
			standardTelephone = new TelephoneOccurrence();

		telephones.remove( telephone );

		if ( telephones.isEmpty() )
			standardTelephone = new TelephoneOccurrence();
	}

	public ContactOccurrence getEntity() {
		return entity;
	}

	public void setEntity( ContactOccurrence entity ) {
		this.entity = entity;
	}

	public List<TelephoneOccurrence> getTelephones() {
		return telephones;
	}

	public void setTelephones( List<TelephoneOccurrence> telephones ) {
		this.telephones = telephones;
	}

	public TelephoneOccurrence getTelephone() {
		return telephone;
	}

	public void setTelephone( TelephoneOccurrence telephone ) {
		this.telephone = telephone;
	}

	public TelephoneOccurrence getStandardTelephone() {
		return standardTelephone;
	}

	public void setStandardTelephone( TelephoneOccurrence standardTelephone ) {
		this.standardTelephone = standardTelephone;
	}

}
