package br.com.talonario.controllers;

import java.io.Serializable;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.inject.Named;

import org.springframework.context.annotation.Scope;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Sort;

import br.com.talonario.entities.Device;
import br.com.talonario.services.DeviceService;

@Named
@Scope( "view" )
public class DeviceManageController extends BaseSearchController<Device> implements Serializable {

	private static final long serialVersionUID = 2881576808376953180L;

	@Inject
	private DeviceService deviceService;

	private Device removeEntity;

	public DeviceManageController() {
		super( Device.class );
	}

	@PostConstruct
	public void init() {
		this.setEditEntity( new Device() );
		this.setRemoveEntity( new Device() );
		this.setSearchEntity( new Device() );
	}

	public void clean() {
		this.setSearchEntity( new Device() );
	}

	@Override
	protected Page<Device> getList( Sort sort ) {
		return new PageImpl<Device>( deviceService.find( getSearchEntity(), sort ) );
	}

	@Override
	public void edit() {
		super.edit();
		this.setEditEntity( deviceService.findOne( getEditEntity().getId() ) );
	}

	public void cancel() {
		closeTab( getEditTab() );
	}

	public void update() {
		Device loadedDevice = deviceService.findDeviceByImei( getEditEntity().getImei() );
		if ( loadedDevice == null )
			deviceService.save( getEditEntity() );
		else {
			FacesContext.getCurrentInstance().addMessage( null, new FacesMessage( FacesMessage.SEVERITY_ERROR, "Erro!", "Dispositivo já existe." ) );
			return;
		}

		FacesContext.getCurrentInstance().addMessage( null, new FacesMessage( "Operação realizada com sucesso" ) );
		closeTab( getEditTab() );
	}

	public void remove() {
		deviceService.delete( removeEntity );
		FacesContext.getCurrentInstance().addMessage( null, new FacesMessage( "Operação realizada com sucesso" ) );
	}

	public String valueEnabled( Boolean enabled ) {
		if ( enabled )
			return "Ativado";
		else
			return "Desativado";

	}

	@Override
	public Device getRemoveEntity() {
		return removeEntity;
	}

	@Override
	public void setRemoveEntity( Device removeEntity ) {
		this.removeEntity = removeEntity;
	}

}
