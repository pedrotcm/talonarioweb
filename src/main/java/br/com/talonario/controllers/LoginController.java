package br.com.talonario.controllers;

import java.io.Serializable;

import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.inject.Named;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.authentication.session.SessionAuthenticationException;
import org.springframework.security.web.authentication.session.SessionAuthenticationStrategy;

import br.com.talonario.config.AdminAuthenticationToken;
import br.com.talonario.entities.User;
import br.com.talonario.services.UserService;

@Named
@Scope( "view" )
public class LoginController implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -6079668084873879841L;

	@Autowired
	private AuthenticationManager authenticationManager;

	@Autowired
	private SessionAuthenticationStrategy sessionAuthenticationStrategy;

	@Inject
	private UserService userService;

	private String username;
	private String password;

	public String login() {
		FacesContext context = FacesContext.getCurrentInstance();

		try {
			Authentication authentication = new AdminAuthenticationToken( getUsername(), getPassword() );

			Authentication result = authenticationManager.authenticate( authentication );
			SecurityContextHolder.getContext().setAuthentication( result );

			HttpServletRequest request = (HttpServletRequest) context.getExternalContext().getRequest();
			HttpServletResponse response = (HttpServletResponse) context.getExternalContext().getResponse();
			sessionAuthenticationStrategy.onAuthentication( result, request, response );

		} catch ( SessionAuthenticationException e ) {
			e.printStackTrace();
			SecurityContextHolder.getContext().getAuthentication().setAuthenticated( false );
			context.addMessage( null, new FacesMessage( FacesMessage.SEVERITY_ERROR, "Usuário já autenticado.", "Usuário já autenticado." ) );
			return null;
		} catch ( AuthenticationException e ) {
			e.printStackTrace();
			context.addMessage( null, new FacesMessage( FacesMessage.SEVERITY_ERROR, e.getMessage(), e.getMessage() ) );
			password = null;
			return null;
		}
		return "/views/index?faces-redirect=true";
	}

	public String getUsername() {
		return username;
	}

	public void setUsername( String userName ) {
		this.username = userName;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword( String password ) {
		this.password = password;
	}

	public User getCurrentUser() {
		return userService.loadCurrentUser();
	}

}
