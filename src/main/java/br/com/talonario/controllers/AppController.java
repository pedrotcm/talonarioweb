package br.com.talonario.controllers;

import java.io.Serializable;

import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.inject.Named;

import org.springframework.context.annotation.Scope;

@Named
@Scope( "singleton" )
public class AppController implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -1151697145001085869L;

	public String getContext() {
		ExternalContext externalContext = FacesContext.getCurrentInstance().getExternalContext();
		String url = externalContext.getRequestContextPath();
		return url;
	}

}
