package br.com.talonario.controllers;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.inject.Named;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Sort;

import br.com.talonario.entities.Organ;
import br.com.talonario.entities.Profile;
import br.com.talonario.entities.User;
import br.com.talonario.enums.ProfileEnum;
import br.com.talonario.repositories.OrganRepository;
import br.com.talonario.services.impl.UserServiceImpl;

@Named
@Scope( "view" )
public class UserManageController extends BaseSearchController<User> implements Serializable {

	private static final long serialVersionUID = 2881576808376953180L;

	@Autowired
	private UserServiceImpl userService;
	@Autowired
	private OrganRepository organRepository;

	private User removeEntity;

	private String password;
	private String confirmPassword;
	private List<Organ> organs;
	private List<Organ> organsEdit;
	private Profile profile;
	private Boolean enabled;

	private Integer userId;

	public UserManageController() {
		super( User.class );
	}

	@PostConstruct
	public void init() {
		this.setEditEntity( new User() );
		this.setRemoveEntity( new User() );
		this.setSearchEntity( new User() );
		this.organs = new ArrayList<Organ>();
		organs = organRepository.findAll();
	}

	public void clean() {
		this.setSearchEntity( new User() );
	}

	@Override
	protected Page<User> getList( Sort sort ) {
		return new PageImpl<User>( userService.find( getSearchEntity(), sort ) );
	}

	@Override
	public void edit() {
		super.edit();
		fillValues( getEditEntity().getId() );
	}

	private void fillValues( Integer userId ) {
		this.setEditEntity( userService.findUserById( userId ) );
		this.profile = getEditEntity().getProfiles().iterator().next();
		this.enabled = getEditEntity().isEnabled();
		this.password = getEditEntity().getPassword().substring( 0, 15 );
		this.confirmPassword = getEditEntity().getPassword().substring( 0, 15 );
		organsEdit = new ArrayList<Organ>();
		for ( Organ organ : getEditEntity().getOrgans() ) {
			organsEdit.add( organ );
		}
	}

	public void cancel() {
		closeTab( getEditTab() );
	}

	public void updateAndClose() {
		update( true );
	}

	public void update() {
		update( false );
	}

	public void update( boolean close ) {
		User loadedUser = userService.findUserById( getEditEntity().getId() );
		User loadedUsernameUser = userService.findUserByUsername( getEditEntity().getUsername() );

		if ( password == null || confirmPassword == null || password.length() < 8 || confirmPassword.length() < 8 ) {
			FacesContext.getCurrentInstance().addMessage( null, new FacesMessage( FacesMessage.SEVERITY_ERROR, "Erro!", "A senha deve ter no mínimo 8 caracteres." ) );
			return;
		} else if ( !password.equals( confirmPassword ) ) {
			FacesContext.getCurrentInstance().addMessage( null, new FacesMessage( FacesMessage.SEVERITY_ERROR, "Erro!", "Confirmar senha diferente da senha." ) );
			return;
		}

		if ( loadedUser.getUsername().equalsIgnoreCase( getEditEntity().getUsername() ) || loadedUsernameUser == null ) {
			getEditEntity().setOrgans( new HashSet<Organ>( organsEdit ) );
			if ( !checkProfileAPP() )
				getEditEntity().setOrgans( new HashSet<Organ>() );
			getEditEntity().setEnabled( enabled );
			Set<Profile> profiles = new HashSet<Profile>();
			profiles.add( profile );
			getEditEntity().setAuthorities( profiles );
			if ( getEditEntity().getPassword().contains( password ) ) {
				userService.save( getEditEntity() );
			} else {
				getEditEntity().setPassword( password );
				userService.create( getEditEntity() );
			}
		} else {
			FacesContext.getCurrentInstance().addMessage( null, new FacesMessage( FacesMessage.SEVERITY_ERROR, "Erro!", "Nome de usuário já existe." ) );
			return;
		}

		FacesContext.getCurrentInstance().addMessage( null, new FacesMessage( "Operação realizada com sucesso" ) );

		fillValues( getEditEntity().getId() );

		if ( close )
			closeTab( getEditTab() );
	}

	public void remove() {
		userService.remove( removeEntity );
		FacesContext.getCurrentInstance().addMessage( null, new FacesMessage( "Operação realizada com sucesso" ) );
	}

	public String valueEnabled( Boolean enabled ) {
		if ( enabled )
			return "Ativado";
		else
			return "Desativado";

	}

	@Override
	public User getRemoveEntity() {
		return removeEntity;
	}

	@Override
	public void setRemoveEntity( User removeEntity ) {
		this.removeEntity = removeEntity;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword( String password ) {
		this.password = password;
	}

	public String getConfirmPassword() {
		return confirmPassword;
	}

	public void setConfirmPassword( String confirmPassword ) {
		this.confirmPassword = confirmPassword;
	}

	public List<Organ> getOrgans() {
		return organs;
	}

	public void setOrgans( List<Organ> organs ) {
		this.organs = organs;
	}

	public List<Organ> getOrgansEdit() {
		return organsEdit;
	}

	public void setOrgansEdit( List<Organ> organsEdit ) {
		this.organsEdit = organsEdit;
	}

	public String getProfileShow( User user ) {
		if ( user != null && user.getAuthorities() != null && user.getAuthorities().iterator().hasNext() )
			return user.getAuthorities().iterator().next().getDescription();
		return null;
	}

	public Boolean checkProfileAPP() {
		if ( profile != null && profile.getAuthority().equals( ProfileEnum.APP.toString() ) )
			return true;
		return false;
	}

	public void cleanProfile() {
		if ( !profile.getDescription().equalsIgnoreCase( "APP" ) ) {
			profile = null;
		}
	}

	public Profile getProfile() {
		return profile;
	}

	public void setProfile( Profile profile ) {
		this.profile = profile;
	}

	public Boolean getEnabled() {
		return enabled;
	}

	public void setEnabled( Boolean enabled ) {
		this.enabled = enabled;
	}

	public String editCurrentUser() {
		return "/views/user/CurrentUserEdit.xhtml?faces-redirect=true&userId=" + userService.loadCurrentUser().getId();
	}

	public Integer getUserId() {
		return userId;
	}

	public void setUserId( Integer userId ) {
		if ( this.userId == null ) {
			this.userId = userId;
			fillValues( userId );
		}
	}

}
