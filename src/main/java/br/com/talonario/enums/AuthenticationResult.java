package br.com.talonario.enums;

public enum AuthenticationResult {

	SUCCESS( 0, "Sucesso" ),
	INCORRECT_USER_PASSWORD( 1, "Usuário ou senha está incorreto." );

	private Integer id;

	private String description;

	private AuthenticationResult( Integer id, String description ) {
		this.id = id;
		this.description = description;
	}

	public Integer getId() {
		return id;
	}

	public String getDescription() {
		return description;
	}

	public static AuthenticationResult fromId( Integer id ) {
		for ( AuthenticationResult mguReturn : values() )
			if ( mguReturn.getId().equals( id ) )
				return mguReturn;
		return null;
	}

	@Override
	public String toString() {
		return description;
	}

}
