package br.com.talonario.services;

import java.util.List;

import br.com.talonario.entities.Organ;
import br.com.talonario.entities.PackageNumberAutos;

public interface PackageNumberService {

	PackageNumberAutos loadPackageNumber( String organName, Integer codeOrgan );

	List<PackageNumberAutos> findByOrganOrderByIdDesc( Organ organ );

	List<PackageNumberAutos> findByOrganAndUsernameAgentOrderByRequestDateDesc( Organ organ, String usernameAgent );

	void save( List<PackageNumberAutos> packageNumber );

}
