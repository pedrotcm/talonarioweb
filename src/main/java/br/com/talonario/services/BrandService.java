package br.com.talonario.services;

import java.util.List;

import br.com.talonario.entities.Brand;

public interface BrandService {

	List<Brand> findAll();

}
