package br.com.talonario.services;

import br.com.talonario.entities.Organ;

public interface OrganService {

	Organ findOrganByNameAndCode( String name, Integer code );

	Organ save( Organ entity );

}
