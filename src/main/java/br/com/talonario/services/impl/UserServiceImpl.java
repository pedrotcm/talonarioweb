package br.com.talonario.services.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Sort;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import br.com.talonario.entities.User;
import br.com.talonario.enums.RoleEnum;
import br.com.talonario.repositories.UserRepository;
import br.com.talonario.services.UserService;

@Service
@Transactional
public class UserServiceImpl implements UserService {

	@Autowired
	private UserRepository userRepository;

	@Override
	@Transactional( readOnly = true )
	public Iterable<User> findAll( Sort sort ) {
		return new PageImpl<User>( userRepository.findAll( sort ) );
	}

	@Override
	@Transactional( readOnly = true )
	public List<User> find( User entity, Sort sort ) {
		String name = new StringBuilder().append( "%" ).append( entity.getName() ).append( "%" ).toString();
		return userRepository.findUsers( name, RoleEnum.ADMIN.toString(), getCurrentUser().getUsername(), sort );
	}

	@Override
	@Transactional( readOnly = true )
	public List<User> findAll() {
		return userRepository.findAll();
	}

	@Override
	public User save( User user ) {
		return userRepository.save( user );
	}

	@Override
	public User create( User user ) {
		BCryptPasswordEncoder passwordEncoder = new BCryptPasswordEncoder( 10 );
		String cryptPassword = passwordEncoder.encode( user.getPassword() );
		user.setPassword( cryptPassword );
		return userRepository.save( user );
	}

	@Override
	@Transactional( readOnly = true )
	public User findUserById( int id ) {
		return userRepository.findOne( id );
	}

	@Override
	public User update( User user ) {
		return userRepository.save( user );
	}

	@Override
	public void remove( User user ) {
		userRepository.delete( user.getId() );
	}

	@Override
	@Transactional( readOnly = true )
	public User findUserByUsername( String username ) {
		return userRepository.findUserByUsername( username );
	}

	@Override
	@Transactional( readOnly = true )
	public User loginAdminGerent( String username ) {
		return userRepository.login( username, RoleEnum.ADMIN.toString(), RoleEnum.GERENT.toString() );
	}

	@Override
	@Transactional( readOnly = true )
	public User loginApp( String username ) {
		return userRepository.loginOneRole( username, RoleEnum.APP.toString() );
	}

	@Override
	@Transactional( readOnly = true )
	public User loginWebService( String username ) {
		return userRepository.loginOneRole( username, RoleEnum.WEB_SERVICE.toString() );
	}

	@Override
	@Transactional( readOnly = true )
	public User findUserByToken( String token ) {
		return userRepository.findUserByToken( token, RoleEnum.APP.toString() );
	}

	@Override
	@Transactional( readOnly = true )
	public User loadCurrentUser() {
		User user = getCurrentUser();
		return user;
	}

	private User getCurrentUser() {
		SecurityContext context = SecurityContextHolder.getContext();
		if ( context == null )
			return null;
		Authentication authentication = context.getAuthentication();
		Object principal = authentication != null ? authentication.getPrincipal() : null;
		if ( !( principal instanceof User ) )
			return null;
		return (User) principal;
	}
}
