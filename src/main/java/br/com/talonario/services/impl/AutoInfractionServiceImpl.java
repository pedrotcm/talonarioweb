package br.com.talonario.services.impl;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import br.com.talonario.entities.AutoInfraction;
import br.com.talonario.repositories.AutoInfractionRepository;
import br.com.talonario.services.AutoInfractionService;

@Service
@Transactional
public class AutoInfractionServiceImpl implements AutoInfractionService {

	@Autowired
	private AutoInfractionRepository autoInfractionRepository;

	@Override
	@Transactional( readOnly = true )
	public List<AutoInfraction> findByCreatedAtBetween( Date initDate, Date endDate ) {
		return autoInfractionRepository.findByCreatedAtBetween( initDate, endDate );
	}

	@Override
	@Transactional( readOnly = true )
	public List<AutoInfraction> findByCreatedAtAfter( Date initDate ) {
		return autoInfractionRepository.findByCreatedAtAfter( initDate );
	}

	@Override
	@Transactional( readOnly = true )
	public List<AutoInfraction> findByCreatedAtBefore( Date endDate ) {
		return autoInfractionRepository.findByCreatedAtBefore( endDate );
	}

	@Override
	public void save( AutoInfraction auto ) {
		autoInfractionRepository.save( auto );
	}

	@Override
	@Transactional( readOnly = true )
	public List<AutoInfraction> findAll() {
		return autoInfractionRepository.findAll();
	}

	@Override
	public AutoInfraction load( Integer id ) {
		return autoInfractionRepository.findOne( id );
	}

}
