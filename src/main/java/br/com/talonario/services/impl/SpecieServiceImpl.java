package br.com.talonario.services.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import br.com.talonario.entities.Specie;
import br.com.talonario.repositories.SpecieRepository;
import br.com.talonario.services.SpecieService;

@Service
@Transactional
public class SpecieServiceImpl implements SpecieService {

	@Autowired
	private SpecieRepository specieRepository;

	@Override
	@Transactional( readOnly = true )
	public List<Specie> findAll() {
		return specieRepository.findAll();
	}

}
