package br.com.talonario.services.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import br.com.talonario.entities.MeasurementType;
import br.com.talonario.repositories.MeasurementTypeRepository;
import br.com.talonario.services.MeasurementTypeService;

@Service
@Transactional
public class MeasureTypeService implements MeasurementTypeService {

	@Autowired
	private MeasurementTypeRepository measureTypeRepository;

	@Override
	@Transactional( readOnly = true )
	public List<MeasurementType> findAll() {
		return measureTypeRepository.findAll();
	}

}
