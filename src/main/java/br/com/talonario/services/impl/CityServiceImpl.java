package br.com.talonario.services.impl;

import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import br.com.talonario.entities.City;
import br.com.talonario.entities.State;
import br.com.talonario.repositories.CityRepository;
import br.com.talonario.services.CityService;

@Service
@Transactional
public class CityServiceImpl implements CityService {

	@Autowired
	private CityRepository cityRepository;

	@Override
	@Transactional( readOnly = true )
	public Set<City> findByStateOrderByNameAsc( State state ) {
		return cityRepository.findByStateOrderByNameAsc( state );
	}

}
