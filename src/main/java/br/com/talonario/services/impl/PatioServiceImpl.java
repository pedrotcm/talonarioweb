package br.com.talonario.services.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import br.com.talonario.entities.Patio;
import br.com.talonario.repositories.PatioRepository;
import br.com.talonario.services.PatioService;

@Service
@Transactional
public class PatioServiceImpl implements PatioService {

	@Autowired
	private PatioRepository patioRepository;

	@Override
	@Transactional( readOnly = true )
	public List<Patio> findAll() {
		return patioRepository.findAll();
	}

}
