package br.com.talonario.services.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import br.com.talonario.entities.ContactOccurrence;
import br.com.talonario.repositories.ContactOccurrenceRepository;
import br.com.talonario.services.ContactOccurrenceService;

@Service
@Transactional
public class ContactOccurrenceServiceImpl implements ContactOccurrenceService {

	@Autowired
	private ContactOccurrenceRepository contactOccurrenceRepository;

	@Override
	@Transactional( readOnly = true )
	public List<ContactOccurrence> find( ContactOccurrence entity, Sort sort ) {
		return contactOccurrenceRepository.findContactOccurrenceByNameContainingIgnoreCase( entity.getName(), sort );
	}

	@Override
	@Transactional( readOnly = true )
	public ContactOccurrence findOne( Integer id ) {
		return contactOccurrenceRepository.findOne( id );
	}

	@Override
	public void save( ContactOccurrence editEntity ) {
		contactOccurrenceRepository.save( editEntity );
	}

	@Override
	public void delete( ContactOccurrence removeEntity ) {
		contactOccurrenceRepository.delete( removeEntity );
	}

	@Override
	@Transactional( readOnly = true )
	public ContactOccurrence findContactOccurrenceByName( String name ) {
		return contactOccurrenceRepository.findContactOccurrenceByNameIgnoreCase( name );
	}

	@Override
	@Transactional( readOnly = true )
	public ContactOccurrence findContactOccurrenceByNameIgnoreCase( String name ) {
		return contactOccurrenceRepository.findContactOccurrenceByNameIgnoreCase( name );
	}

	@Override
	@Transactional( readOnly = true )
	public List<ContactOccurrence> findAll() {
		return contactOccurrenceRepository.findAll();
	}

}
