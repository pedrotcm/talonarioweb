package br.com.talonario.services.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import br.com.talonario.entities.Brand;
import br.com.talonario.repositories.BrandRepository;
import br.com.talonario.services.BrandService;

@Service
@Transactional
public class BrandServiceImpl implements BrandService {

	@Autowired
	private BrandRepository brandRepository;

	@Override
	@Transactional( readOnly = true )
	public List<Brand> findAll() {
		return brandRepository.findAll();
	}

}
