package br.com.talonario.services.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import br.com.talonario.entities.Infraction;
import br.com.talonario.repositories.InfractionRepository;
import br.com.talonario.services.InfractionService;

@Service
@Transactional
public class InfractionServiceImpl implements InfractionService {

	@Autowired
	private InfractionRepository infractionRepository;

	@Override
	@Transactional( readOnly = true )
	public List<Infraction> findAll() {
		return infractionRepository.findAll();
	}

}
