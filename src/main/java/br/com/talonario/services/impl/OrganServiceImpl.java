package br.com.talonario.services.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import br.com.talonario.entities.Organ;
import br.com.talonario.repositories.OrganRepository;
import br.com.talonario.services.OrganService;

@Service
@Transactional
public class OrganServiceImpl implements OrganService {

	@Autowired
	private OrganRepository organRepository;

	@Override
	@Transactional( readOnly = true )
	public Organ findOrganByNameAndCode( String name, Integer code ) {
		return organRepository.findOrganByNameAndCode( name, code );
	}

	@Override
	public Organ save( Organ entity ) {
		return organRepository.save( entity );
	}

}
