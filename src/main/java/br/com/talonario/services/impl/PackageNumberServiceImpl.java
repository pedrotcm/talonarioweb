package br.com.talonario.services.impl;

import java.util.List;

import javax.inject.Inject;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import br.com.talonario.entities.Organ;
import br.com.talonario.entities.PackageNumberAutos;
import br.com.talonario.entities.SettingParameter;
import br.com.talonario.entities.User;
import br.com.talonario.repositories.OrganRepository;
import br.com.talonario.repositories.PackageNumberAutosRepository;
import br.com.talonario.repositories.SettingParameterRepository;
import br.com.talonario.services.PackageNumberService;
import br.com.talonario.services.UserService;

@Service
@Transactional
public class PackageNumberServiceImpl implements PackageNumberService {

	@Autowired
	private PackageNumberAutosRepository packageNumberAutosRepository;

	@Autowired
	private OrganRepository organRepository;

	@Autowired
	private SettingParameterRepository settingRepository;

	@Inject
	private UserService userService;

	@Override
	public synchronized PackageNumberAutos loadPackageNumber( String organName, Integer codeOrgan ) {
		List<SettingParameter> settings = settingRepository.findAll();
		int qntAutosInPackage = settings.get( 0 ).getQntAutoInPackage();

		PackageNumberAutos packageNumberAutos;
		PackageNumberAutos newPackage;

		User currentUser = userService.loadCurrentUser();

		// Authentication authentication =
		// SecurityContextHolder.getContext().getAuthentication();
		// User currentUser = userRepository.findUserByUsername(
		// authentication.getName() );

		Organ organ = organRepository.findOrganByNameAndCode( organName, codeOrgan );

		Pageable page = new PageRequest( 0, 1, Direction.DESC, "requestDate" );
		Page<PackageNumberAutos> packageAll = packageNumberAutosRepository.findAll( page );
		PackageNumberAutos currentPackage = null;
		if ( !packageAll.getContent().isEmpty() )
			currentPackage = packageAll.getContent().get( 0 );

		List<PackageNumberAutos> packageNumber = packageNumberAutosRepository.findByOrganAndUsernameAgentOrderByRequestDateDesc( organ, currentUser.getUsername() );
		if ( packageAll.getContent().isEmpty() ) {
			packageNumberAutos = new PackageNumberAutos();
			packageNumberAutos.setInitNumber( 1 );
			packageNumberAutos.setEndNumber( qntAutosInPackage );
			packageNumberAutos.setCurrentNumber( 0 );
			packageNumberAutos.setOrgan( organ );
			packageNumberAutos.setUsernameAgent( currentUser.getUsername() );
			packageNumberAutosRepository.save( packageNumberAutos );
			packageNumberAutos.setOrgan( null );
			return packageNumberAutos;
		} else if ( packageNumber.isEmpty() ) {
			packageNumberAutos = new PackageNumberAutos();
			packageNumberAutos.setInitNumber( currentPackage.getEndNumber() + 1 );
			packageNumberAutos.setEndNumber( currentPackage.getEndNumber() + qntAutosInPackage );
			packageNumberAutos.setCurrentNumber( 0 );
			packageNumberAutos.setOrgan( organ );
			packageNumberAutos.setUsernameAgent( currentUser.getUsername() );
			packageNumberAutosRepository.save( packageNumberAutos );
			packageNumberAutos.setOrgan( null );
			return packageNumberAutos;
		} else if ( packageNumber.get( 0 ).getCurrentNumber() >= ( packageNumber.get( 0 ).getEndNumber() ) ) {
			newPackage = new PackageNumberAutos();
			newPackage.setInitNumber( currentPackage.getEndNumber() + 1 );
			newPackage.setEndNumber( currentPackage.getEndNumber() + qntAutosInPackage );
			newPackage.setCurrentNumber( 0 );
			newPackage.setOrgan( organ );
			newPackage.setUsernameAgent( currentUser.getUsername() );
			packageNumberAutosRepository.save( newPackage );
			newPackage.setOrgan( null );
			return newPackage;
		} else {
			packageNumberAutos = packageNumber.get( 0 );
			packageNumberAutos.setOrgan( null );
			return packageNumberAutos;
		}
	}

	@Override
	@Transactional( readOnly = true )
	public List<PackageNumberAutos> findByOrganOrderByIdDesc( Organ organ ) {
		return packageNumberAutosRepository.findByOrganOrderByIdDesc( organ );
	}

	@Override
	@Transactional( readOnly = true )
	public List<PackageNumberAutos> findByOrganAndUsernameAgentOrderByRequestDateDesc( Organ organ, String usernameAgent ) {
		return packageNumberAutosRepository.findByOrganAndUsernameAgentOrderByRequestDateDesc( organ, usernameAgent );
	}

	@Override
	public void save( List<PackageNumberAutos> packageNumber ) {
		packageNumberAutosRepository.save( packageNumber );
	}
}
