package br.com.talonario.services.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import br.com.talonario.entities.SettingParameter;
import br.com.talonario.repositories.SettingParameterRepository;
import br.com.talonario.services.SettingParameterService;

@Service
@Transactional
public class SettingParameterServiceImpl implements SettingParameterService {

	@Autowired
	private SettingParameterRepository settingParameterRepository;

	@Override
	@Transactional( readOnly = true )
	public List<Object> findImagePath() {
		return settingParameterRepository.findImagePath();
	}

	@Override
	@Transactional( readOnly = true )
	public List<Object> findBaseUrlDownload() {
		return settingParameterRepository.findBaseUrlDownload();
	}

	@Override
	@Transactional( readOnly = true )
	public List<SettingParameter> findAll() {
		return settingParameterRepository.findAll();
	}

	@Override
	public void save( SettingParameter entity ) {
		settingParameterRepository.save( entity );
	}

	@Override
	@Transactional( readOnly = true )
	public SettingParameter findOne( Integer id ) {
		return settingParameterRepository.findOne( id );
	}

}
