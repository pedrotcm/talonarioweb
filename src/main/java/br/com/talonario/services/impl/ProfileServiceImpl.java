package br.com.talonario.services.impl;

import java.util.Collection;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import br.com.talonario.entities.Profile;
import br.com.talonario.repositories.ProfileRepository;
import br.com.talonario.services.ProfileService;

@Service
@Transactional
public class ProfileServiceImpl implements ProfileService {

	@Autowired
	private ProfileRepository profileRepository;

	@Override
	@Transactional( readOnly = true )
	public List<Profile> findByDescriptionIn( Collection<String> descriptions ) {
		return profileRepository.findByDescriptionIn( descriptions );
	}

	@Override
	@Transactional( readOnly = true )
	public Profile findProfileByDescription( String description ) {
		return profileRepository.findProfileByDescription( description );
	}

	@Override
	@Transactional( readOnly = true )
	public List<Profile> findByDescriptionNot( String description ) {
		return profileRepository.findByDescriptionNot( description );
	}

}
