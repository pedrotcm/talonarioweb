package br.com.talonario.services.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import br.com.talonario.entities.Device;
import br.com.talonario.repositories.DeviceRepository;
import br.com.talonario.services.DeviceService;

@Service
@Transactional
public class DeviceServiceImpl implements DeviceService {

	@Autowired
	private DeviceRepository deviceRepository;

	@Override
	@Transactional( readOnly = true )
	public List<Device> find( Device entity, Sort sort ) {
		return deviceRepository.findByImeiContainingIgnoreCase( entity.getImei(), sort );
	}

	@Override
	@Transactional( readOnly = true )
	public Device findOne( Integer id ) {
		return deviceRepository.findOne( id );
	}

	@Override
	@Transactional( readOnly = true )
	public Device findDeviceByImei( String imei ) {
		return deviceRepository.findDeviceByImei( imei );
	}

	@Override
	public void save( Device editEntity ) {
		deviceRepository.save( editEntity );
	}

	@Override
	public void delete( Device removeEntity ) {
		deviceRepository.delete( removeEntity );
	}

	@Override
	public Device findDeviceByAgentUsername( String username ) {
		return deviceRepository.findDeviceByAgentUsername( username );
	}

	@Override
	public Device findDeviceByImeiAndAgentUsername( String imei, String username ) {
		return deviceRepository.findDeviceByImeiAndAgentUsername( imei, username );
	}

}
