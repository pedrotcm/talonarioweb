package br.com.talonario.services.impl;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.UUID;

import javax.inject.Inject;

import org.jasypt.encryption.pbe.StandardPBEStringEncryptor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import br.com.talonario.entities.User;
import br.com.talonario.services.TokenService;
import br.com.talonario.services.UserService;

@Service
@Transactional
public class TokenServiceImpl implements TokenService {

	@Autowired
	private StandardPBEStringEncryptor jasypt;

	@Inject
	private UserService userService;

	@Override
	@Transactional( readOnly = true )
	public String generateToken( String username, Date creationDateTime ) {
		SimpleDateFormat dateFormat = new SimpleDateFormat( "yyyy-MM-dd HH:mm:ss" );
		String createdAt = dateFormat.format( creationDateTime );

		String key = UUID.randomUUID().toString().toUpperCase() + "-" + username + "-" + createdAt;
		key.replaceAll( "\\\\", "-" );
		String authenticationToken = jasypt.encrypt( key );

		return authenticationToken;
	}

	@Override
	@Transactional( readOnly = true )
	public String getToken( UserDetails userDetails ) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	@Transactional( readOnly = true )
	public String getToken( UserDetails userDetails, Long expiration ) {
		return null;
	}

	@Override
	@Transactional( readOnly = true )
	public boolean validate( String token ) {
		User user = userService.findUserByToken( token );
		if ( user != null )
			return true;
		return false;
	}

	@Override
	@Transactional( readOnly = true )
	public UserDetails getUserFromToken( String token ) {
		User user = userService.findUserByToken( token );
		return user;
	}

}
