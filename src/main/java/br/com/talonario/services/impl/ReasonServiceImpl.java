package br.com.talonario.services.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import br.com.talonario.entities.Reason;
import br.com.talonario.repositories.ReasonRepository;
import br.com.talonario.services.ReasonService;

@Service
@Transactional
public class ReasonServiceImpl implements ReasonService {

	@Autowired
	private ReasonRepository reasonRepository;

	@Override
	@Transactional( readOnly = true )
	public List<Reason> findAll() {
		return reasonRepository.findAll();
	}

}
