package br.com.talonario.services.impl;

import java.io.File;
import java.io.IOException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.text.SimpleDateFormat;
import java.util.Collection;
import java.util.Date;
import java.util.Formatter;

import org.apache.commons.io.FileUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import br.com.talonario.entities.Image;
import br.com.talonario.repositories.SettingParameterRepository;
import br.com.talonario.services.ImageService;

@Service
@Transactional
public class ImageServiceImpl implements ImageService {

	@Autowired
	private SettingParameterRepository settingParameterRepository;

	@Override
	public void save( Collection<Image> images ) {
		if ( images != null && !images.isEmpty() ) {
			for ( Image image : images ) {
				save( image );
			}
		}

	}

	private void save( Image image ) {
		if ( image.getFileContent() != null ) {
			create( image );
		}
	}

	private void create( Image image ) {
		try {
			byte[] content = image.getFileContent();
			String filename = byteArray2Hex( content );
			// String filename = image.getName() + image.getExtension();

			// String realPath = servletContext.getRealPath( "/" );
			String directory = getFilePath( "/imagens" ) + File.separator + new SimpleDateFormat( "yyyyMMdd" ).format( new Date() );
			File diskFile = new File( directory, filename );
			FileUtils.writeByteArrayToFile( diskFile, content );
			String urlFile = getBaseUrlDownloader() + File.separator + diskFile.getAbsolutePath().replace( getFilePath( "" ), "" );
			image.setUrl( urlFile );
			image.setPath( diskFile.getAbsolutePath() );
		} catch ( IOException e ) {
			e.printStackTrace();
		} catch ( NoSuchAlgorithmException e ) {
			e.printStackTrace();
		}
	}

	private String getFilePath( String dir ) {
		String imagePath = (String) settingParameterRepository.findImagePath().get( 0 );
		return imagePath + File.separator + dir;
	}

	private String getBaseUrlDownloader() {
		String imagePath = (String) settingParameterRepository.findBaseUrlDownload().get( 0 );
		return imagePath;
	}

	private static String byteArray2Hex( final byte[] hash ) throws NoSuchAlgorithmException {
		MessageDigest md = MessageDigest.getInstance( "SHA-1" );
		byte[] data = md.digest( hash );
		String result = null;
		Formatter formatter = new Formatter();
		for ( byte b : data ) {
			formatter.format( "%02x", b );
		}
		result = formatter.toString();
		formatter.close();
		return result;
	}

}
