package br.com.talonario.services;

import java.util.List;

import br.com.talonario.entities.MeasurementType;

public interface MeasurementTypeService {

	List<MeasurementType> findAll();

}
