package br.com.talonario.services;

import java.util.List;

import org.springframework.data.domain.Sort;

import br.com.talonario.entities.Device;

public interface DeviceService {

	List<Device> find( Device entity, Sort sort );

	Device findOne( Integer id );

	Device findDeviceByImei( String imei );

	void save( Device editEntity );

	void delete( Device removeEntity );

	Device findDeviceByAgentUsername( String username );

	Device findDeviceByImeiAndAgentUsername( String imei, String username );

}
