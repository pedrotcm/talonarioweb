package br.com.talonario.services;

import java.util.Date;
import java.util.List;

import br.com.talonario.entities.AutoInfraction;

public interface AutoInfractionService {

	List<AutoInfraction> findByCreatedAtBetween( Date initDate, Date endDate );

	List<AutoInfraction> findByCreatedAtAfter( Date initDate );

	List<AutoInfraction> findByCreatedAtBefore( Date endDate );

	void save( AutoInfraction auto );

	List<AutoInfraction> findAll();

	AutoInfraction load( Integer id );

}
