package br.com.talonario.services;

import java.util.Set;

import br.com.talonario.entities.City;
import br.com.talonario.entities.State;

public interface CityService {

	Set<City> findByStateOrderByNameAsc( State state );

}
