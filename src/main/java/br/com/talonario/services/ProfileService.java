package br.com.talonario.services;

import java.util.Collection;
import java.util.List;

import br.com.talonario.entities.Profile;

public interface ProfileService {

	List<Profile> findByDescriptionIn( Collection<String> descriptions );

	Profile findProfileByDescription( String description );

	List<Profile> findByDescriptionNot( String description );
}
