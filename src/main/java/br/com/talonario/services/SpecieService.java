package br.com.talonario.services;

import java.util.List;

import br.com.talonario.entities.Specie;

public interface SpecieService {

	List<Specie> findAll();

}
