package br.com.talonario.services;

import java.util.List;

import br.com.talonario.entities.Reason;

public interface ReasonService {

	List<Reason> findAll();

}
