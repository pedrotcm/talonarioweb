package br.com.talonario.services;

import java.util.List;

import org.springframework.data.domain.Sort;

import br.com.talonario.entities.ContactOccurrence;

public interface ContactOccurrenceService {

	List<ContactOccurrence> find( ContactOccurrence entity, Sort sort );

	ContactOccurrence findOne( Integer id );

	void save( ContactOccurrence editEntity );

	void delete( ContactOccurrence removeEntity );

	ContactOccurrence findContactOccurrenceByName( String name );

	ContactOccurrence findContactOccurrenceByNameIgnoreCase( String name );

	List<ContactOccurrence> findAll();

}
