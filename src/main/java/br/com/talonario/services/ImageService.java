package br.com.talonario.services;

import java.util.Collection;

import br.com.talonario.entities.Image;

public interface ImageService {

	void save( Collection<Image> images );

}
