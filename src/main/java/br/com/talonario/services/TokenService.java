package br.com.talonario.services;

import java.util.Date;

import org.springframework.security.core.userdetails.UserDetails;

public interface TokenService {

	String generateToken( String username, Date creationDateTime );

	String getToken( UserDetails userDetails );

	String getToken( UserDetails userDetails, Long expiration );

	boolean validate( String token );

	UserDetails getUserFromToken( String token );
}