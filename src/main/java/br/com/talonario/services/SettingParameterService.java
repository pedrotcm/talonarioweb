package br.com.talonario.services;

import java.util.List;

import br.com.talonario.entities.SettingParameter;

public interface SettingParameterService {

	List<Object> findImagePath();

	List<Object> findBaseUrlDownload();

	List<SettingParameter> findAll();

	void save( SettingParameter entity );

	SettingParameter findOne( Integer id );
}
