package br.com.talonario.services;

import java.util.List;

import br.com.talonario.entities.Patio;

public interface PatioService {

	List<Patio> findAll();

}
