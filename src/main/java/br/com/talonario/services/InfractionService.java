package br.com.talonario.services;

import java.util.List;

import br.com.talonario.entities.Infraction;

public interface InfractionService {

	List<Infraction> findAll();

}
