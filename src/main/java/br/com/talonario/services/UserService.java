package br.com.talonario.services;

import java.util.List;

import org.springframework.data.domain.Sort;

import br.com.talonario.entities.User;

public interface UserService {

	Iterable<User> findAll( Sort sort );

	List<User> find( User entity, Sort sort );

	List<User> findAll();

	User save( User user );

	User create( User user );

	User findUserById( int id );

	User update( User user );

	void remove( User user );

	User findUserByUsername( String username );

	User loginAdminGerent( String username );

	User loginApp( String username );

	User loginWebService( String username );

	User findUserByToken( String token );

	User loadCurrentUser();

}
