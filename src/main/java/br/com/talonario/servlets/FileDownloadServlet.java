package br.com.talonario.servlets;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.xml.bind.DatatypeConverter;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.stereotype.Component;
import org.springframework.web.HttpRequestHandler;
import org.springframework.web.context.support.HttpRequestHandlerServlet;

import br.com.talonario.config.WebServiceAuthenticationProvider;
import br.com.talonario.config.WebServiceAuthenticationToken;
import br.com.talonario.entities.Image;
import br.com.talonario.repositories.ImageRepository;
import br.com.talonario.repositories.SettingParameterRepository;

@Component( "fileDownloadServlet" )
public class FileDownloadServlet extends HttpRequestHandlerServlet implements HttpRequestHandler {

	private static final long serialVersionUID = 1L;

	private static final String PARAM_USER = "user";
	private static final String PARAM_PASSWORD = "pass";

	@Autowired
	private WebServiceAuthenticationProvider authManager;

	@Autowired
	private SettingParameterRepository settingParameterRepository;

	@Autowired
	private ImageRepository imageRepository;

	@Override
	public void handleRequest( HttpServletRequest req, HttpServletResponse resp ) throws ServletException, IOException {
		String authorization = req.getHeader( "Authorization" );
		String[] credentials;

		if ( authorization == null ) {
			credentials = new String[] { req.getParameter( PARAM_USER ), req.getParameter( PARAM_PASSWORD ) };
			if ( credentials[0] == null || credentials[1] == null ) {
				resp.setHeader( "WWW-Authenticate", "BASIC realm=\"Talonário Image Server\"" );
				resp.sendError( HttpServletResponse.SC_UNAUTHORIZED );
				credentials = null;
			}
		} else {
			credentials = new String( DatatypeConverter.parseBase64Binary( authorization.replace( "Basic ", "" ) ) ).split( ":", 2 );
		}

		if ( credentials != null ) {
			if ( authUser( credentials[0], credentials[1] ) ) {
				sendImage( req, resp );
			} else {
				resp.getOutputStream().println( "Unauthorized: Invalid authentication." );
				resp.setStatus( HttpServletResponse.SC_FORBIDDEN );
			}
		}
	}

	private boolean authUser( String username, String password ) {
		Authentication authResult;
		UsernamePasswordAuthenticationToken authentication = new WebServiceAuthenticationToken( username, password );
		try {
			authResult = authManager.authenticate( authentication );
		} catch ( AuthenticationException e ) {
			return false;
		}
		return authResult != null;
	}

	private void sendImage( HttpServletRequest req, HttpServletResponse resp ) throws FileNotFoundException, IOException {
		String imagePath = (String) settingParameterRepository.findImagePath().get( 0 );
		if ( imagePath.endsWith( "//" ) ) {
			imagePath = imagePath.substring( 0, imagePath.length() - 1 );
		}
		String filePath = imagePath + "/imagens" + File.separator + req.getPathInfo().substring( 1 );

		List<Image> image = imageRepository.findImageByPath( filePath );
		if ( image != null && !image.isEmpty() ) {

			File file = new File( filePath );
			if ( file.exists() ) {
				resp.addHeader( "Content-Disposition", String.format( "filename=\"%s\"", image.get( 0 ).getName() + image.get( 0 ).getExtension() ) );
				// resp.addHeader( "Content-Type", "application/octet-stream" );
				// baixar
				// resp.addHeader( "Content-Type", "image/jpeg" );
				BufferedInputStream buffStream = new BufferedInputStream( new FileInputStream( file ) );
				try {
					byte[] buff = new byte[1024];
					int count;
					while ( ( count = buffStream.read( buff ) ) != -1 ) {
						resp.getOutputStream().write( buff, 0, count );
					}
				} finally {
					buffStream.close();
				}
			} else {
				resp.getOutputStream().println( "Image does not exist." );
				resp.setStatus( HttpServletResponse.SC_ACCEPTED );
			}
		} else {
			resp.getOutputStream().println( "Image not found." );
			resp.setStatus( HttpServletResponse.SC_NOT_FOUND );
		}
	}
}
