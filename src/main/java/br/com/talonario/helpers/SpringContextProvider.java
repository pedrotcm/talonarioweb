package br.com.talonario.helpers;

import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.stereotype.Component;

@Component
public class SpringContextProvider implements ApplicationContextAware {

	private static ApplicationContext context;

	@Override
	public void setApplicationContext( ApplicationContext applicationContext ) throws BeansException {
		SpringContextProvider.setContext( applicationContext );
	}

	public static ApplicationContext getContext() {
		return context;
	}

	private static void setContext( ApplicationContext context ) {
		SpringContextProvider.context = context;
	}

}
