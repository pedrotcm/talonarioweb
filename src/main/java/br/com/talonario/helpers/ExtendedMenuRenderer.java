package br.com.talonario.helpers;

import java.util.List;

import org.primefaces.component.panelmenu.PanelMenuRenderer;
import org.primefaces.model.menu.MenuElement;
import org.primefaces.model.menu.MenuGroup;
import org.primefaces.model.menu.MenuItem;

public class ExtendedMenuRenderer extends PanelMenuRenderer {
	@Override
	protected MenuItem findMenuitem( List<MenuElement> elements, String id ) {
		if ( elements == null || elements.isEmpty() ) {
			return null;
		} else {
			if ( id.startsWith( SEPARATOR ) ) {
				id = id.substring( id.indexOf( SEPARATOR ) + 1 );
			}
			String[] paths = id.split( SEPARATOR );

			if ( paths.length == 0 )
				return null;

			int childIndex = 0;
			try {
				childIndex = Integer.parseInt( paths[0] );

			} catch ( Exception e ) {
				return null;
			}
			if ( childIndex >= elements.size() )
				return null;

			MenuElement childElement = elements.get( childIndex );

			if ( paths.length == 1 ) {
				return (MenuItem) childElement;
			} else {
				String relativeIndex = id.substring( id.indexOf( SEPARATOR ) + 1 );

				return findMenuitem( ( (MenuGroup) childElement ).getElements(), relativeIndex );
			}
		}
	}
}