package br.com.talonario.helpers;

import java.util.List;

/**
 * Representação do menu a partir do arquivo .yaml
 * 
 * @author fernando
 */
public class MenuTreeNode {

	private String description;

	private String url;

	private String icon;

	private List<MenuTreeNode> children;
	private String code;

	public String getDescription() {
		return description;
	}

	public void setDescription( String description ) {
		this.description = description;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl( String url ) {
		this.url = url;
	}

	public String getIcon() {
		return icon;
	}

	public void setIcon( String icon ) {
		this.icon = icon;
	}

	public String getCode() {
		return code;
	}

	public void setCode( String code ) {
		this.code = code;
	}

	public List<MenuTreeNode> getChildren() {
		return children;
	}

	public void setChildren( List<MenuTreeNode> children ) {
		this.children = children;
	}

	@Override
	public String toString() {
		return description;
	}
}