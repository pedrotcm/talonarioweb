package br.com.talonario.helpers;

import java.util.List;

import org.primefaces.model.menu.BaseMenuModel;
import org.primefaces.model.menu.MenuElement;
import org.primefaces.model.menu.MenuGroup;

public class MyDefaultMenuModel extends BaseMenuModel {

	private static final long serialVersionUID = 7894666404556382443L;
	private boolean generated = false;

	@Override
	public void generateUniqueIds() {
		if ( !generated ) {
			generateUniqueIds( getElements(), "_" );
			generated = true;
		}
	}

	private void generateUniqueIds( List<MenuElement> elements, String seed ) {
		if ( elements == null || elements.isEmpty() ) {
			return;
		}

		int counter = 0;

		for ( MenuElement element : elements ) {
			String id = ( ( seed == null ) ? String.valueOf( counter++ ) : seed );
			if ( seed != "_" ) {
				id += "_";
			}
			id += counter++;
			element.setId( id );

			if ( element instanceof MenuGroup ) {
				generateUniqueIds( ( (MenuGroup) element ).getElements(), id );
			}
		}
	}
}