package br.com.talonario.utils;

import java.text.Normalizer;

public class StringUtils {
	public static String removeAccents( String str ) {
		if ( str == null )
			return str;
		String result = Normalizer.normalize( str, Normalizer.Form.NFD );
		result = result.replaceAll( "[^\\p{ASCII}]", "" );
		return result;
	}

	public static String buildRoleName( String processId, String nodeName ) {
		return String.format( "ROLE_%s_%s", processId, nodeName ).replaceAll( "(\\s+|\\.)", "_" ).toUpperCase();
	}
}