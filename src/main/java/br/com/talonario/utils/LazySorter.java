package br.com.talonario.utils;

import java.lang.reflect.Field;
import java.util.Comparator;

import org.primefaces.model.SortOrder;

public class LazySorter<T> implements Comparator<T> {

	private String sortField;

	private SortOrder sortOrder;

	public LazySorter( String sortField, SortOrder sortOrder ) {
		this.sortField = sortField;
		this.sortOrder = sortOrder;
	}

	public int compare( T obj1, T obj2 ) {
		try {
			Class<?> klass = obj1.getClass();
			Field field = klass.getDeclaredField( this.sortField ); // NoSuchFieldException
			field.setAccessible( true );

			Object value1 = field.get( obj1 );
			Object value2 = field.get( obj2 );

			int value = ( (Comparable) value1 ).compareTo( value2 );

			return SortOrder.ASCENDING.equals( sortOrder ) ? value : -1 * value;
		} catch ( Exception e ) {
			throw new RuntimeException();
		}
	}
}