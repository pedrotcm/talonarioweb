package br.com.talonario.config;

import javax.inject.Named;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.AuthenticationServiceException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

import br.com.talonario.entities.User;
import br.com.talonario.services.UserService;

@Named
public class AdminAuthenticationProvider implements AuthenticationProvider {

	@Autowired
	private UserService userService;

	public AdminAuthenticationProvider() {
		super();
	}

	@Override
	public Authentication authenticate( Authentication authentication ) throws AuthenticationException {
		BCryptPasswordEncoder passwordEncoder = new BCryptPasswordEncoder( 10 );

		String username = (String) authentication.getPrincipal();
		String password = (String) authentication.getCredentials();

		if ( username == null || username.trim().length() == 0 ) {
			throw new AuthenticationServiceException( "Campo usuário é obrigatório" );
		}

		if ( password == null || password.trim().length() == 0 ) {
			throw new AuthenticationServiceException( "Campo senha é obrigatória" );
		}

		User usuario = userService.loginAdminGerent( username );
		try {
			if ( usuario != null ) {
				if ( passwordEncoder.matches( password, usuario.getPassword() ) ) {
					return new UsernamePasswordAuthenticationToken( usuario, usuario.getPassword(), usuario.getAuthorities() );
				} else if ( usuario.getPassword().equals( password ) ) {
					return new UsernamePasswordAuthenticationToken( usuario, usuario.getPassword(), usuario.getAuthorities() );
				}
				throw new AuthenticationServiceException( "Usuário ou senha está incorreto" );
			} else {
				throw new AuthenticationServiceException( "Usuário ou senha está incorreto" );
			}

		} catch ( AuthenticationServiceException e ) {
			throw e;
		} catch ( Throwable e ) {
			throw new AuthenticationServiceException( "Ocorreu um erro no ato da autenticação", e );
		}
	}

	@Override
	public boolean supports( Class<? extends Object> authentication ) {
		return AdminAuthenticationToken.class.isAssignableFrom( authentication ) && authentication.equals( AdminAuthenticationToken.class );
	}

}