package br.com.talonario.config;

import javax.inject.Inject;
import javax.inject.Named;

import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.AuthenticationServiceException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;

import br.com.talonario.entities.User;
import br.com.talonario.services.UserService;

@Named
public class AppAuthenticationProvider implements AuthenticationProvider {

	@Inject
	private UserService userService;

	public AppAuthenticationProvider() {
		super();
	}

	@Override
	public Authentication authenticate( Authentication authentication ) throws AuthenticationException {
		String username = (String) authentication.getPrincipal();
		String password = (String) authentication.getCredentials();

		User usuario = userService.loginApp( username );
		try {
			if ( usuario != null ) {
				if ( usuario.getPassword().equals( password ) ) {
					return new UsernamePasswordAuthenticationToken( usuario, usuario.getPassword(), usuario.getAuthorities() );
				}
				throw new AuthenticationServiceException( "Usuário ou senha está incorreto" );
			} else {
				throw new AuthenticationServiceException( "Usuário ou senha está incorreto" );
			}

		} catch ( AuthenticationServiceException e ) {
			throw e;
		} catch ( Throwable e ) {
			throw new AuthenticationServiceException( "Ocorreu um erro no ato da autenticação", e );
		}
	}

	@Override
	public boolean supports( Class<? extends Object> authentication ) {
		return AppAuthenticationToken.class.isAssignableFrom( authentication ) && authentication.equals( AppAuthenticationToken.class );
	}

}