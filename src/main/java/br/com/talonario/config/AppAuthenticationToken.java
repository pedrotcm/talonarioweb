package br.com.talonario.config;

import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;

public class AppAuthenticationToken extends UsernamePasswordAuthenticationToken {

	private static final long serialVersionUID = 1691285764969639989L;

	public AppAuthenticationToken( Object principal, Object credentials ) {
		super( principal, credentials );
	}
}