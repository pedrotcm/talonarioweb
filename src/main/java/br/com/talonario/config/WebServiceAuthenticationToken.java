package br.com.talonario.config;

import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;

public class WebServiceAuthenticationToken extends UsernamePasswordAuthenticationToken {

	private static final long serialVersionUID = 1691285764969639989L;

	public WebServiceAuthenticationToken( Object principal, Object credentials ) {
		super( principal, credentials );
	}
}