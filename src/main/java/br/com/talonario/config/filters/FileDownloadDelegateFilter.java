package br.com.talonario.config.filters;

import java.io.IOException;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;

import org.springframework.web.filter.GenericFilterBean;

public class FileDownloadDelegateFilter extends GenericFilterBean {

	private String contextPath;
	
	public void setContextPath( String contextPath ) {
		this.contextPath = contextPath;
	}

	@Override
	public void doFilter( ServletRequest request, ServletResponse response, FilterChain chain ) throws IOException, ServletException {
		HttpServletRequest httpRequest = (HttpServletRequest) request;
		String path = httpRequest.getRequestURI().replace( httpRequest.getContextPath(), "" );
		
		if (path.startsWith( contextPath )) {
			request.getRequestDispatcher( path ).forward( request, response );
		} else {
			chain.doFilter( request, response );
		}
	}

}
