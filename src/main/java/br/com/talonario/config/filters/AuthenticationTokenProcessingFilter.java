package br.com.talonario.config.filters;

import java.io.IOException;

import javax.inject.Inject;
import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import jersey.repackaged.com.google.common.base.Function;
import jersey.repackaged.com.google.common.collect.Collections2;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.web.authentication.AbstractAuthenticationProcessingFilter;
import org.springframework.security.web.authentication.WebAuthenticationDetailsSource;

import br.com.talonario.config.AppAuthenticationToken;
import br.com.talonario.config.WebServiceAuthenticationToken;
import br.com.talonario.services.TokenService;

public class AuthenticationTokenProcessingFilter extends AbstractAuthenticationProcessingFilter {

	private static final String DEFAULT_FILTER_PROCESSES_URL = "/login.xhtml";

	public static final Logger logger = LoggerFactory.getLogger( AuthenticationTokenProcessingFilter.class );

	@Inject
	private TokenService tokenUtils;

	public AuthenticationTokenProcessingFilter() {
		super( DEFAULT_FILTER_PROCESSES_URL );
	}

	@Override
	public void doFilter( ServletRequest request, ServletResponse response, FilterChain chain ) throws IOException, ServletException {
		// Map<String, String[]> parms = request.getParameterMap();

		HttpServletRequest req = (HttpServletRequest) request;
		HttpServletResponse resp = (HttpServletResponse) response;

		String username = req.getParameter( "username" );
		String password = req.getParameter( "password" );
		String token = req.getHeader( "Token" );

		if ( req.getRequestURI().equalsIgnoreCase( "/talonario/login.xhtml" ) ) {
			Authentication authResult = SecurityContextHolder.getContext().getAuthentication();
			if ( authResult != null && authResult.isAuthenticated() )
				successfulAuthentication( (HttpServletRequest) request, (HttpServletResponse) response, chain, authResult );
		}

		String viewPath = req.getRequestURI().replace( req.getContextPath(), "" );
		if ( !isAuthenticated() && !isLoginPage( viewPath ) && isWebService( viewPath ) ) {
			if ( isWebServiceTalonario( viewPath ) && username != null && password != null ) {
				try {
					logger.info( "WebService Authentication..." );
					logger.info( "Accessing {}", viewPath );
					UsernamePasswordAuthenticationToken authentication = new WebServiceAuthenticationToken( username, password );
					authentication.setDetails( new WebAuthenticationDetailsSource().buildDetails( (HttpServletRequest) request ) );
					SecurityContextHolder.getContext().setAuthentication( getAuthenticationManager().authenticate( authentication ) );
					chain.doFilter( request, response );
					return;
				} catch ( AuthenticationException e ) {
					logger.info( e.getMessage() );
					resp.setContentType( "text/html; charset=UTF-8" );
					resp.getWriter().println( e.getMessage() );
					return;
				} finally {
					logger.info( "Logout WebService Authentication..." );
					SecurityContextHolder.getContext().setAuthentication( null );
				}
			}
			if ( token != null ) {
				// validate the token
				if ( tokenUtils.validate( token ) ) {
					try {
						logger.info( "App Authentication..." );
						logger.info( "Accessing {}", viewPath );
						// determine the user based on the (already validated)
						// token
						UserDetails userDetails = tokenUtils.getUserFromToken( token );
						// build an Authentication object with the user's info
						UsernamePasswordAuthenticationToken authentication = new AppAuthenticationToken( userDetails.getUsername(), userDetails.getPassword() );
						authentication.setDetails( new WebAuthenticationDetailsSource().buildDetails( (HttpServletRequest) request ) );
						SecurityContextHolder.getContext().setAuthentication( getAuthenticationManager().authenticate( authentication ) );
						chain.doFilter( request, response );
						return;
					} catch ( AuthenticationException e ) {
						logger.info( e.getMessage() );
						resp.setContentType( "text/html; charset=UTF-8" );
						resp.getWriter().println( e.getMessage() );
						return;
					} finally {
						logger.info( "Logout App Authentication..." );
						SecurityContextHolder.getContext().setAuthentication( null );
					}
				}
			}
		}

		// continue thru the filter chain
		chain.doFilter( request, response );
	}

	private boolean isWebService( String viewPath ) {
		return viewPath.startsWith( "/ws" );
	}

	private boolean isWebServiceTalonario( String viewPath ) {
		return viewPath.startsWith( "/ws/resource" );
	}

	private boolean isLoginPage( String viewPath ) {
		return viewPath.equals( "/login.xhtml" );
	}

	private boolean isAuthenticated() {
		Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
		if ( authentication == null )
			return false;
		return !Collections2.transform( authentication.getAuthorities(), new Function<GrantedAuthority, String>() {
			@Override
			public String apply( GrantedAuthority arg0 ) {
				return arg0.getAuthority();
			}
		} ).contains( "ROLE_ANONYMOUS" );
	}

	@Override
	public Authentication attemptAuthentication( HttpServletRequest request, HttpServletResponse response ) throws AuthenticationException, IOException, ServletException {
		return null;
	}

}