package br.com.talonario.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import br.com.talonario.entities.SettingParameter;

public interface SettingParameterRepository extends JpaRepository<SettingParameter, Integer> {

	@Query( "SELECT imagePath FROM SettingParameter ORDER BY id DESC" )
	public List<Object> findImagePath();

	@Query( "SELECT baseDownloadUrl FROM SettingParameter ORDER BY id DESC" )
	public List<Object> findBaseUrlDownload();
}
