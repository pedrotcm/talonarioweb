package br.com.talonario.repositories;

import java.util.List;

import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import br.com.talonario.entities.User;

public interface UserRepository extends JpaRepository<User, Integer> {

	@Query( "select u from User u inner join u.profiles p where u.username = :username and (p.role = :roleOne or p.role = :roleTwo)" )
	User login( @Param( "username" ) String username, @Param( "roleOne" ) String roleOne, @Param( "roleTwo" ) String roleTwo );

	@Query( "select u from User u inner join u.profiles p where u.username = :username and p.role = :roleOne" )
	User loginOneRole( @Param( "username" ) String username, @Param( "roleOne" ) String roleOne );

	User findUserByUsername( String username );

	@Query( "select u from User u inner join u.profiles p where u.token = :token and p.role = :roleOne" )
	User findUserByToken( @Param( "token" ) String token, @Param( "roleOne" ) String roleOne );

	List<User> findByNameContainingIgnoreCase( String name, Sort sort );

	@Query( "select u from User u left join u.profiles p where u.name LIKE :name and p.role != :role and u.username != :currentUser" )
	List<User> findUsers( @Param( "name" ) String name, @Param( "role" ) String role, @Param( "currentUser" ) String currentUser, Sort sort );

}
