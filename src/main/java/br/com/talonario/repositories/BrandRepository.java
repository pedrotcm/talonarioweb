package br.com.talonario.repositories;

import org.springframework.data.jpa.repository.JpaRepository;

import br.com.talonario.entities.Brand;

public interface BrandRepository extends JpaRepository<Brand, Integer> {

}
