package br.com.talonario.repositories;

import java.util.Set;

import org.springframework.data.jpa.repository.JpaRepository;

import br.com.talonario.entities.City;
import br.com.talonario.entities.State;

public interface CityRepository extends JpaRepository<City, Integer> {

	Set<City> findByStateOrderByNameAsc( State state );

}
