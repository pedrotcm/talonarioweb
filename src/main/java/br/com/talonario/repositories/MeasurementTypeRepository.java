package br.com.talonario.repositories;

import org.springframework.data.jpa.repository.JpaRepository;

import br.com.talonario.entities.MeasurementType;

public interface MeasurementTypeRepository extends JpaRepository<MeasurementType, Integer> {

}
