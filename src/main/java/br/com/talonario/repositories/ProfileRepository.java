package br.com.talonario.repositories;

import java.util.Collection;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import br.com.talonario.entities.Profile;

public interface ProfileRepository extends JpaRepository<Profile, Integer> {

	List<Profile> findByDescriptionIn( Collection<String> descriptions );

	Profile findProfileByDescription( String description );

	List<Profile> findByDescriptionNot( String description );
}
