package br.com.talonario.repositories;

import org.springframework.data.jpa.repository.JpaRepository;

import br.com.talonario.entities.Organ;

public interface OrganRepository extends JpaRepository<Organ, Integer> {

	Organ findOrganByNameAndCode( String name, Integer code );
}
