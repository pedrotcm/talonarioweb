package br.com.talonario.repositories;

import org.springframework.data.jpa.repository.JpaRepository;

import br.com.talonario.entities.Reason;

public interface ReasonRepository extends JpaRepository<Reason, Integer> {

}
