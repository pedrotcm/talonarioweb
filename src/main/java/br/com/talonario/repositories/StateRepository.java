package br.com.talonario.repositories;

import java.util.List;

import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.repository.JpaRepository;

import br.com.talonario.entities.State;

public interface StateRepository extends JpaRepository<State, Integer> {

	List<State> findByUfContainingIgnoreCase( String uf, Sort sort );
}
