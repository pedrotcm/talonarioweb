package br.com.talonario.repositories;

import org.springframework.data.jpa.repository.JpaRepository;

import br.com.talonario.entities.Specie;

public interface SpecieRepository extends JpaRepository<Specie, Integer> {

}
