package br.com.talonario.repositories;

import org.springframework.data.jpa.repository.JpaRepository;

import br.com.talonario.entities.Patio;

public interface PatioRepository extends JpaRepository<Patio, Integer> {

}
