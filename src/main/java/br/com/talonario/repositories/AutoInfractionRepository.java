package br.com.talonario.repositories;

import java.util.Date;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import br.com.talonario.entities.AutoInfraction;

public interface AutoInfractionRepository extends JpaRepository<AutoInfraction, Integer> {

	List<AutoInfraction> findByCreatedAtBetween( Date initDate, Date endDate );

	List<AutoInfraction> findByCreatedAtAfter( Date initDate );

	List<AutoInfraction> findByCreatedAtBefore( Date endDate );
}
