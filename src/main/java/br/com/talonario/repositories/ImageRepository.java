package br.com.talonario.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import br.com.talonario.entities.Image;

public interface ImageRepository extends JpaRepository<Image, Integer> {

	List<Image> findImageByPath( String path );

}
