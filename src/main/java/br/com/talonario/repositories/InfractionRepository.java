package br.com.talonario.repositories;

import org.springframework.data.jpa.repository.JpaRepository;

import br.com.talonario.entities.Infraction;

public interface InfractionRepository extends JpaRepository<Infraction, Integer> {

}
