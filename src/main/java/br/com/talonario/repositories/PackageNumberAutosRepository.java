package br.com.talonario.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import br.com.talonario.entities.Organ;
import br.com.talonario.entities.PackageNumberAutos;

public interface PackageNumberAutosRepository extends JpaRepository<PackageNumberAutos, Integer> {

	List<PackageNumberAutos> findByOrganOrderByIdDesc( Organ organ );

	List<PackageNumberAutos> findByOrganAndUsernameAgentOrderByRequestDateDesc( Organ organ, String usernameAgent );

}
