package br.com.talonario.repositories;

import java.util.List;

import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.repository.JpaRepository;

import br.com.talonario.entities.ContactOccurrence;

public interface ContactOccurrenceRepository extends JpaRepository<ContactOccurrence, Integer> {

	ContactOccurrence findContactOccurrenceByNameIgnoreCase( String name );

	List<ContactOccurrence> findContactOccurrenceByNameContainingIgnoreCase( String name, Sort sort );

}
