package br.com.talonario.repositories;

import java.util.List;

import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.repository.JpaRepository;

import br.com.talonario.entities.Device;

public interface DeviceRepository extends JpaRepository<Device, Integer> {

	Device findDeviceByImei( String imei );

	List<Device> findByImeiContainingIgnoreCase( String imei, Sort sort );

	Device findDeviceByAgentUsername( String agentUsername );

	Device findDeviceByImeiAndAgentUsername( String imei, String agentUsername );

}
